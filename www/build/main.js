webpackJsonp([13],{

/***/ 105:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return ChatPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(9);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__providers_rest_rest__ = __webpack_require__(13);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};



/**
 * Generated class for the ChatPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */
var ChatPage = (function () {
    function ChatPage(navCtrl, navParams, restProvider, alertCtrl, toastCtrl) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.restProvider = restProvider;
        this.alertCtrl = alertCtrl;
        this.toastCtrl = toastCtrl;
        this.offline = 3;
    }
    ChatPage.prototype.loadchat = function () {
        var _this = this;
        this.restProvider.getMessages()
            .then(function (result) {
            if (result['error']) {
                _this.presentToast("Something goes wrong");
            }
            else {
                /* Array.prototype.sort.call(result, function (a, b) {
                     if (a.chat_id > b.chat_id) {
                         return 1;
                     }
                     if (a.chat_id < b.chat_id) {
                         return -1;
                     }
                     return 0;
                 });*/
                _this.messages = result;
            }
        }, function (err) {
            console.log(err);
        });
    };
    ChatPage.prototype.ionViewDidLoad = function () {
        console.log('ionViewDidLoad ChatPage');
    };
    ChatPage.prototype.ionViewWillEnter = function () {
        var _this = this;
        this.restProvider.getChatStatus()
            .then(function (result) {
            if (result['available'] > 0) {
                if (_this.restProvider.thread_id == 'none') {
                    _this.offline = 2;
                }
                else {
                    _this.offline = 0;
                    _this.loadchat();
                }
            }
            else {
                _this.offline = 1;
            }
        }, function (err) {
            console.log(err);
        });
    };
    ChatPage.prototype.sendFirstMsg = function () {
        var _this = this;
        this.restProvider.sendMessage(this.message_data)
            .then(function (result) {
            if (result['error']) {
                _this.presentToast("Something goes wrong");
            }
            else {
                _this.presentToast(result['msg']);
                _this.message_data = "";
                _this.restProvider.thread_id = result['thread_id'];
                _this.offline = 0;
                _this.loadchat();
            }
        }, function (err) {
            console.log(err);
        });
    };
    ChatPage.prototype.sendmsg = function () {
        var _this = this;
        this.restProvider.sendMessage(this.message_data)
            .then(function (result) {
            if (result['error']) {
                _this.presentToast("Something goes wrong");
            }
            else {
                _this.presentToast(result['msg']);
                _this.message_data = "";
                _this.loadchat();
            }
        }, function (err) {
            console.log(err);
        });
    };
    ChatPage.prototype.presentToast = function (msg) {
        var toast = this.toastCtrl.create({
            message: msg,
            duration: 5000,
            position: 'top'
        });
        toast.present();
    };
    ChatPage.prototype.doRefresh = function (refresher) {
        var _this = this;
        this.restProvider.getChatStatus()
            .then(function (result) {
            if (result['available'] > 0) {
                if (_this.restProvider.thread_id == 'none') {
                    _this.offline = 2;
                }
                else {
                    _this.offline = 0;
                    _this.loadchat();
                }
            }
            else {
                _this.offline = 1;
            }
            refresher.complete();
        }, function (err) {
            console.log(err);
            refresher.complete();
        });
    };
    ChatPage.prototype.closeChat = function () {
        var _this = this;
        var alert = this.alertCtrl.create({
            title: 'Confirm Closing',
            message: 'Do you want to close this chat?',
            buttons: [
                {
                    text: 'No',
                    role: 'cancel',
                    handler: function () {
                        console.log('no clicked');
                    }
                },
                {
                    text: 'Yes',
                    handler: function () {
                        _this.restProvider.closeThread().then(function (result) {
                            _this.presentToast(result['msg']);
                            _this.message_data = "";
                            _this.restProvider.thread_id = 'none';
                            _this.offline = 2;
                        });
                    }
                }
            ]
        });
        alert.present();
    };
    ChatPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'page-chat',template:/*ion-inline-start:"/Users/admin/Desktop/kitclient/src/pages/chat/chat.html"*/'<ion-content align="center" padding>\n        <ion-img class="Logo" align="center" src="assets/kitvision.png"></ion-img>\n        <hr class="logohr">\n        <h2><ion-label align="center" TextDecorations="Underline">CHAT</ion-label>\n        </h2>\n    <hr class="logohr">\n        <ion-refresher (ionRefresh)="doRefresh($event)">\n            <ion-refresher-content\n                    pullingIcon="arrow-dropdown"\n                    pullingText="Pull to refresh"\n                    refreshingSpinner="circles"\n                    refreshingText="Loading...">\n            </ion-refresher-content>\n        </ion-refresher>\n        <div class="chat-box" *ngIf="offline==0">\n            <div class="close-chat" (click)="closeChat()">\n                Close Chat\n                <ion-icon name="close-circle" style="color: #FA4E06;font-size:30px"></ion-icon>\n            </div>\n\n            <ion-list class="list">\n                <ion-item class="item" *ngFor="let message of messages">\n                    <ion-avatar item-start>\n                        <img src="{{(message.user.avatar == null || message.user.avatar == \'\' )?\'assets/avatar.png\': message.user.avatar}}">\n                      </ion-avatar>\n                    <h2>{{message.user.name}}</h2>\n                    <p class="sp0" >{{message.message}} </p>\n                </ion-item>\n            </ion-list>\n\n            <div class="message_send_container">\n                <textarea class="description" placeholder="" [(ngModel)]="message_data"></textarea>\n                <div  class="btnSend">\n                    <button (click)="sendmsg()"  class="sendBtn">SEND</button>\n                </div>\n            </div>\n        </div>\n\n\n    <div class="chat-box" *ngIf="offline==2">\n        <div class="chat_heading_desc">Need Instant Help?<br/>Chat with one of our technicians?</div>\n        <textarea class="description" placeholder="Type your message here..." [(ngModel)]="message_data" style="height:200px;"></textarea>\n\n        <div  class="btnSend">\n            <button (click)="sendFirstMsg()"  class="sendBtn">START CHAT</button>\n        </div>\n    </div>\n\n    <div class="offline" *ngIf="offline==1">\n        <br><br>\n        <ion-thumbnail class="offlineChat">\n            <img src="assets/offlinechat.png">\n        </ion-thumbnail>\n        <br><br>\n        <ion-label>NO ONE IS ONLINE</ion-label>\n        <ion-label>PLEASE SEND EMAIL ON</ion-label>\n        <ion-label ><h3><u>md@kitvision.net</u></h3></ion-label>\n    </div>\n </ion-content>\n'/*ion-inline-end:"/Users/admin/Desktop/kitclient/src/pages/chat/chat.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["h" /* NavController */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["i" /* NavParams */],
            __WEBPACK_IMPORTED_MODULE_2__providers_rest_rest__["a" /* RestProvider */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["a" /* AlertController */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["k" /* ToastController */]])
    ], ChatPage);
    return ChatPage;
}());

//# sourceMappingURL=chat.js.map

/***/ }),

/***/ 106:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return DashboardPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(9);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__trb_shoot_trb_shoot__ = __webpack_require__(107);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__chat_chat__ = __webpack_require__(105);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__support_support__ = __webpack_require__(108);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__history_history__ = __webpack_require__(109);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__providers_rest_rest__ = __webpack_require__(13);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7__profile_profile__ = __webpack_require__(110);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_8__ionic_native_push__ = __webpack_require__(172);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};









/**
 * Generated class for the DashboardPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */
var DashboardPage = (function () {
    // tab6Root = LoginPage;
    function DashboardPage(navCtrl, navParams, restProvider, push, alertCtrl) {
        var _this = this;
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.restProvider = restProvider;
        this.push = push;
        this.alertCtrl = alertCtrl;
        this.tab1Root = __WEBPACK_IMPORTED_MODULE_2__trb_shoot_trb_shoot__["a" /* TrbShootPage */];
        this.tab2Root = __WEBPACK_IMPORTED_MODULE_3__chat_chat__["a" /* ChatPage */];
        this.tab3Root = __WEBPACK_IMPORTED_MODULE_4__support_support__["a" /* SupportPage */];
        this.tab4Root = __WEBPACK_IMPORTED_MODULE_5__history_history__["a" /* HistoryPage */];
        this.tab5Root = __WEBPACK_IMPORTED_MODULE_7__profile_profile__["a" /* ProfilePage */];
        this.restProvider.setChatStatus(1)
            .then(function (result) {
            //do nothing
        }, function (err) {
            console.log(err);
        });
        this.initPush();
        // to check if we have permission
        /*
            this.push.hasPermission()
                .then((res: any) => {
      
                    if (res.isEnabled) {
                        this.initPush();
                    } else {
                        console.log('We do not have permission to send push notifications');
                    }
      
                });
      */
        //for chat status
        this.task = setInterval(function () {
            _this.restProvider.setChatStatus(1);
        }, 30000);
    }
    DashboardPage.prototype.initPush = function () {
        var _this = this;
        var options = {
            android: {
                senderID: '800934935444',
            },
            ios: {
                alert: 'true',
                badge: true,
                sound: 'false'
            },
            windows: {},
            browser: {
                pushServiceURL: 'http://push.api.phonegap.com/v1/push',
                applicationServerKey: 'AIzaSyB1IB67RCeadDQ-R27weVrUdzn9tv3VFJg',
            }
        };
        var pushObject = this.push.init(options);
        pushObject.on('notification').subscribe(function (notification) {
            /*if(notification.message.indexOf('Chat') != -1)
                this.presentChatAlert(notification.message);
            else if(notification.message.indexOf('Ticket') != -1)
                this.presentTicketAlert(notification.message);
            else*/
            _this.presentAlert(notification.message, notification.title);
        });
        pushObject.on('registration').subscribe(function (registration) {
            _this.restProvider.setChatToken(registration.registrationId).then(function (result) {
                console.log(result['msg']);
            });
            console.log('Device registered', registration.registrationId);
        });
        pushObject.on('error').subscribe(function (error) { return console.error('Error with Push plugin', error); });
    };
    DashboardPage.prototype.presentAlert = function (msg, title) {
        var alert = this.alertCtrl.create({
            title: title,
            subTitle: msg,
            buttons: ['Dismiss']
        });
        alert.present();
    };
    DashboardPage.prototype.ionViewDidLoad = function () {
        console.log('ionViewDidLoad DashboardPage');
    };
    DashboardPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'page-dashboard',template:/*ion-inline-start:"/Users/admin/Desktop/kitclient/src/pages/dashboard/dashboard.html"*/'  \n    <ion-tabs  color="tabs" class="tabs" selectedIndex="2" >\n      <ion-tab [root]="tab1Root"text-wrap tabTitle="FILES" tabIcon="troubleshoot"></ion-tab>\n      <ion-tab [root]="tab2Root" tabTitle="CHAT" tabIcon="chat"></ion-tab>\n      <ion-tab [root]="tab3Root" tabTitle="SUPPORT" tabIcon="support"></ion-tab>\n      <ion-tab [root]="tab4Root" tabTitle="HISTORY" tabIcon="history"></ion-tab>\n      <ion-tab [root]="tab5Root" tabTitle="PROFILE" tabIcon="profile"></ion-tab>\n      </ion-tabs>'/*ion-inline-end:"/Users/admin/Desktop/kitclient/src/pages/dashboard/dashboard.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["h" /* NavController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["i" /* NavParams */], __WEBPACK_IMPORTED_MODULE_6__providers_rest_rest__["a" /* RestProvider */],
            __WEBPACK_IMPORTED_MODULE_8__ionic_native_push__["a" /* Push */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["a" /* AlertController */]])
    ], DashboardPage);
    return DashboardPage;
}());

//# sourceMappingURL=dashboard.js.map

/***/ }),

/***/ 107:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return TrbShootPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(9);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__providers_rest_rest__ = __webpack_require__(13);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};



/**
 * Generated class for the TrbShootPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */
var TrbShootPage = (function () {
    function TrbShootPage(navCtrl, navParams, restProvider, toastCtrl) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.restProvider = restProvider;
        this.toastCtrl = toastCtrl;
        this.searchTerm = '';
        this.currentUserData = JSON.parse(localStorage.getItem('currentUser'));
        this.getAllFiles(this.currentUserData.token);
    }
    TrbShootPage.prototype.ionViewDidLoad = function () {
        console.log('ionViewDidLoad TrbShootPage');
    };
    TrbShootPage.prototype.getAllFiles = function (token) {
        var _this = this;
        this.restProvider.getFiles(token)
            .then(function (result) {
            if (result['error']) {
                _this.presentToast("Something goes wrong");
            }
            else {
                _this.files = result;
                _this.allFiles = result;
            }
        }, function (err) {
            console.log(err);
        });
    };
    TrbShootPage.prototype.presentToast = function (msg) {
        var toast = this.toastCtrl.create({
            message: msg,
            duration: 5000
        });
        toast.present();
    };
    TrbShootPage.prototype.filterItems = function (searchTerm) {
        //this.getAllFiles(this.currentUserData.token);
        return this.files.filter(function (item) {
            return item.file_name.toLowerCase().indexOf(searchTerm.toLowerCase()) > -1;
        });
    };
    TrbShootPage.prototype.setFilteredItems = function () {
        this.files = this.allFiles;
        //this.getAllFiles(this.currentUserData.token);
        this.files = this.filterItems(this.searchTerm);
    };
    TrbShootPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'page-trb-shoot',template:/*ion-inline-start:"/Users/admin/Desktop/kitclient/src/pages/trb-shoot/trb-shoot.html"*/'\n<ion-content align="center" padding>\n    <ion-img class="Logo" align="center" src="assets/kitvision.png"></ion-img>\n    <hr class="logohr">\n    <h2 class="imp"><ion-label align="center" TextDecorations="Underline">TROUBLESHOOT FILES</ion-label>\n    </h2><hr class="logohr">\n\n\n\n\n  <ion-searchbar class="searchbar"\n  [(ngModel)]="searchTerm" (ionInput)="setFilteredItems()" \n  placeholder="Search Our Library">\n</ion-searchbar>\n<br/>\n<ion-list class="list">\n    <ion-item *ngFor="let file of files">\n      <ion-thumbnail item-start class="docImage">\n          <img class="docImage" src="assets/pdf.png" alt="{{file.file_name}}" />\n      </ion-thumbnail>\n      <p class="file-desc"> {{ file.file_name }}</p>\n      <a href="{{ file.path }}"><ion-thumbnail item-end class="downImage"><img src="assets/download.png"></ion-thumbnail></a>\n    </ion-item>\n</ion-list>\n</ion-content>\n    '/*ion-inline-end:"/Users/admin/Desktop/kitclient/src/pages/trb-shoot/trb-shoot.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["h" /* NavController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["i" /* NavParams */], __WEBPACK_IMPORTED_MODULE_2__providers_rest_rest__["a" /* RestProvider */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["k" /* ToastController */]])
    ], TrbShootPage);
    return TrbShootPage;
}());

//# sourceMappingURL=trb-shoot.js.map

/***/ }),

/***/ 108:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return SupportPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(9);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__verify_support_verify_support__ = __webpack_require__(52);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__providers_rest_rest__ = __webpack_require__(13);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};




/**
 * Generated class for the SupportPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */
var SupportPage = (function () {
    function SupportPage(navCtrl, navParams, restProvider, toastCtrl, alertCtrl, app, loadingCtrl) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.restProvider = restProvider;
        this.toastCtrl = toastCtrl;
        this.alertCtrl = alertCtrl;
        this.app = app;
        this.loadingCtrl = loadingCtrl;
        this.ticket = {
            "api_token": "",
            "screen_id": "",
            "prob_desc": ""
        };
        this.currentUserData = JSON.parse(localStorage.getItem('currentUser'));
        this.ticket.api_token = this.currentUserData.token;
        this.getAllScreens(this.currentUserData.token);
    }
    SupportPage.prototype.verifySupport = function () {
        this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_2__verify_support_verify_support__["a" /* VerifySupportPage */]);
    };
    SupportPage.prototype.ionViewDidLoad = function () {
        console.log('ionViewDidLoad SupportPage');
    };
    SupportPage.prototype.getAllScreens = function (token) {
        var _this = this;
        this.restProvider.getScreens(token)
            .then(function (data) {
            _this.screens = data;
        });
    };
    SupportPage.prototype.addUserTicket = function () {
        var _this = this;
        if (this.ticket.screen_id == "") {
            var alert_1 = this.alertCtrl.create({
                title: 'Choose Screen',
                subTitle: 'Please choose screen',
                buttons: ['Dismiss']
            });
            alert_1.present();
            return;
        }
        if (this.ticket.prob_desc == "") {
            var alert_2 = this.alertCtrl.create({
                title: 'Add Description',
                subTitle: 'Please Add problem Description',
                buttons: ['Dismiss']
            });
            alert_2.present();
            return;
        }
        var loading = this.loadingCtrl.create({
            content: 'Please wait...'
        });
        loading.present();
        this.restProvider.addTicket(this.ticket)
            .then(function (result) {
            if (result['error']) {
                _this.presentToast("Something goes wrong");
            }
            else {
                _this.presentToast(result['msg']);
                _this.ticket.prob_desc = '';
            }
            loading.dismiss();
        }, function (err) {
            console.log(err);
        });
    };
    SupportPage.prototype.presentToast = function (msg) {
        var _this = this;
        var toast = this.toastCtrl.create({
            message: msg,
            duration: 5000,
            position: 'top'
        });
        toast.onDidDismiss(function () {
            _this.app.getRootNav().getActiveChildNav().select(3);
        });
        toast.present();
    };
    SupportPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'page-support',template:/*ion-inline-start:"/Users/admin/Desktop/kitclient/src/pages/support/support.html"*/'<ion-content align="center" padding="20px">\n    <ion-img class="Logo" align="center" src="assets/kitvision.png"></ion-img>\n    <hr class="logohr">\n    <h2><ion-label align="center" TextDecorations="Underline">GET INSTANT HELP</ion-label>\n    </h2><hr class="logohr">\n    <ion-img class="imgFwd"  style="float: center;" src="assets/fd.png"></ion-img>\n    \n    <ion-item style="border: 0.5px solid black; padding-left:3px;" class="multiPicker">\n        <ion-label style="padding-left: 10px;">Choose Screen</ion-label>\n        <ion-select [(ngModel)]="ticket.screen_id" placeholder="Choose Your Screen" class="select-screen" >\n          <ion-option *ngFor="let screen of screens" value="{{ screen.screenID }}">{{ screen.screen_name }}</ion-option>\n        </ion-select>\n    </ion-item>\n    <ion-item class="problem-wrapper">\n        <ion-textarea class="problem-desc" placeholder="Describe Your Problem (Max 120 words)"  [(ngModel)]="ticket.prob_desc" maxlength="120"></ion-textarea>\n    </ion-item>\n  \n    \n    <button round class="tecBtn" (click)="addUserTicket()" >REQUEST A TECHNICIAN</button>\n</ion-content>\n'/*ion-inline-end:"/Users/admin/Desktop/kitclient/src/pages/support/support.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["h" /* NavController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["i" /* NavParams */],
            __WEBPACK_IMPORTED_MODULE_3__providers_rest_rest__["a" /* RestProvider */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["k" /* ToastController */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["a" /* AlertController */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["b" /* App */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["g" /* LoadingController */]])
    ], SupportPage);
    return SupportPage;
}());

//# sourceMappingURL=support.js.map

/***/ }),

/***/ 109:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return HistoryPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(9);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__providers_rest_rest__ = __webpack_require__(13);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__verify_support_verify_support__ = __webpack_require__(52);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__support_tracker_support_tracker__ = __webpack_require__(53);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__feedback_feedback__ = __webpack_require__(54);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};






/**
 * Generated class for the HistoryPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */
var HistoryPage = (function () {
    function HistoryPage(navCtrl, navParams, restProvider, toastCtrl, alertCtrl) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.restProvider = restProvider;
        this.toastCtrl = toastCtrl;
        this.alertCtrl = alertCtrl;
        /*this.currentUserData = JSON.parse(localStorage.getItem('currentUser'));
            this.getAllHistory(this.currentUserData.token);*/
    }
    HistoryPage.prototype.ionViewDidLoad = function () {
        console.log('ionViewDidLoad HistoryPage');
    };
    HistoryPage.prototype.ionViewWillEnter = function () {
        this.currentUserData = JSON.parse(localStorage.getItem('currentUser'));
        this.getAllHistory(this.currentUserData.token);
    };
    HistoryPage.prototype.getAllHistory = function (token) {
        var _this = this;
        this.restProvider.getHistory(token)
            .then(function (result) {
            if (result['error']) {
                _this.presentToast("Something goes wrong");
            }
            else {
                _this.history = result;
            }
        }, function (err) {
            console.log(err);
        });
    };
    HistoryPage.prototype.presentToast = function (msg) {
        var toast = this.toastCtrl.create({
            message: msg,
            duration: 2000,
            position: 'top'
        });
        toast.present();
    };
    HistoryPage.prototype.ticketclick = function (ticket_id, approved, tech_id, feedback) {
        if (approved == 1) {
            this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_3__verify_support_verify_support__["a" /* VerifySupportPage */], { 'ticket_id': ticket_id, 'tech_id': tech_id });
        }
        else if (approved == 2) {
            this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_4__support_tracker_support_tracker__["a" /* SupportTrackerPage */], { 'ticket_id': ticket_id, 'tech_id': tech_id });
        }
        else if (approved == 3) {
            if (feedback == 0)
                this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_5__feedback_feedback__["a" /* FeedbackPage */], { 'ticket_id': ticket_id, 'tech_id': tech_id });
            else {
                var alert_1 = this.alertCtrl.create({
                    title: 'Feedback Given',
                    subTitle: 'Feedback already given against this ticket',
                    buttons: ['Dismiss']
                });
                alert_1.present();
            }
        }
    };
    HistoryPage.prototype.doRefresh = function (refresher) {
        this.getAllHistory(this.currentUserData.token);
        refresher.complete();
    };
    HistoryPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'page-history',template:/*ion-inline-start:"/Users/admin/Desktop/kitclient/src/pages/history/history.html"*/'<ion-content align="center" padding>\n        <ion-img class="Logo" align="center" src="assets/kitvision.png"></ion-img>\n        <hr class="logohr">\n        <h2><ion-label align="center" TextDecorations="Underline">TICKETS HISTORY</ion-label>\n        </h2><hr class="logohr">\n\n        <ion-refresher (ionRefresh)="doRefresh($event)">\n            <ion-refresher-content\n                    pullingIcon="arrow-dropdown"\n                    pullingText="Pull to refresh"\n                    refreshingSpinner="circles"\n                    refreshingText="Refreshing...">\n            </ion-refresher-content>\n        </ion-refresher>\n\n       <div class="cardContainer" *ngFor="let ticket of history" (click)="ticketclick(ticket.ticket_id,ticket.approved,ticket.assign_to,ticket.feedback)">\n                       <!--<div class="cardlabel2">-->\n                       <div class="cardlabel2P2">\n                           <ion-thumbnail class="profileImage">\n                               <img src="{{ticket.avatar == null ?\'assets/avatar.png\':ticket.avatar}}" >\n                           </ion-thumbnail>\n                           <ion-label class="profiletexts" >{{ ticket.assigned_user == null ?"Not Assigned":ticket.assigned_user }} - {{ticket.phone == null ?"No Number":ticket.phone}}</ion-label>\n                           <ion-label class="profiletexts">{{ ticket.created_at | date: \'dd-M-yyyy\'}}</ion-label>\n                       </div>\n                       <!--</div>-->\n                       <div class="cardlabel">\n                             <ion-label class="ticket"><font color="red"><strong>ID# {{ ticket.ticket_id }}</strong></font></ion-label>\n                           <ion-label class="date"  ion-text [style.color]="ticket.color"><strong>{{ticket.status}}</strong></ion-label>\n                        </div>\n                       <div class="cardlabel" *ngIf="ticket.remote_area==1">\n                           <ion-label class="ticket">Schedule Date</ion-label>\n                           <ion-label class="date">{{ticket.schedule_date}}</ion-label>\n                       </div>\n                       <div class="cardlabel">\n                           <ion-label class="ticket">{{ ticket.screen_name }}</ion-label>\n                           <ion-label class="date">\n                               <div class="rating">\n                                   <ion-icon name="star" *ngIf="ticket.rating > 0"></ion-icon>\n                                   <ion-icon name="star" *ngIf="ticket.rating > 1"></ion-icon>\n                                   <ion-icon name="star" *ngIf="ticket.rating > 2"></ion-icon>\n                                   <ion-icon name="star" *ngIf="ticket.rating > 3"></ion-icon>\n                                   <ion-icon name="star" *ngIf="ticket.rating > 4"></ion-icon>\n                                   <ion-icon name="star-outline" *ngIf="ticket.rating < 1"></ion-icon>\n                                   <ion-icon name="star-outline" *ngIf="ticket.rating < 2"></ion-icon>\n                                   <ion-icon name="star-outline" *ngIf="ticket.rating < 3"></ion-icon>\n                                   <ion-icon name="star-outline" *ngIf="ticket.rating < 4"></ion-icon>\n                                   <ion-icon name="star-outline" *ngIf="ticket.rating < 5"></ion-icon>\n                               </div>\n                           </ion-label>\n                       </div>\n                        <!--<hr class="hrLine">-->\n\n                            <!--<hr class="ratingline">-->\n                        <!--<div class="personDetails">\n                            <ion-label class="ratingLabel" >Rating</ion-label>\n                            <div class="rating">\n                                <ion-icon name="star" *ngIf="ticket.rating > 0"></ion-icon>\n                                <ion-icon name="star" *ngIf="ticket.rating > 1"></ion-icon>\n                                <ion-icon name="star" *ngIf="ticket.rating > 2"></ion-icon>\n                                <ion-icon name="star" *ngIf="ticket.rating > 3"></ion-icon>\n                                <ion-icon name="star" *ngIf="ticket.rating > 4"></ion-icon>\n                                <ion-icon name="star-outline" *ngIf="ticket.rating < 1"></ion-icon>\n                                <ion-icon name="star-outline" *ngIf="ticket.rating < 2"></ion-icon>\n                                <ion-icon name="star-outline" *ngIf="ticket.rating < 3"></ion-icon>\n                                <ion-icon name="star-outline" *ngIf="ticket.rating < 4"></ion-icon>\n                                <ion-icon name="star-outline" *ngIf="ticket.rating < 5"></ion-icon>\n\n\n                            </div>\n                            </div>-->\n                             <!--<hr class="ratingline">-->\n                       \n                    <div class="cardContent">\n                        <div class="message">\n                          <p>Main Message</p>\n                        </div>\n                        <div class="messagedetails">\n                          <p>{{ ticket.problem_desc }}\n                          </p>\n                        </div>\n                    </div>\n       </div>\n\n              \n\n</ion-content>'/*ion-inline-end:"/Users/admin/Desktop/kitclient/src/pages/history/history.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["h" /* NavController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["i" /* NavParams */], __WEBPACK_IMPORTED_MODULE_2__providers_rest_rest__["a" /* RestProvider */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["k" /* ToastController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["a" /* AlertController */]])
    ], HistoryPage);
    return HistoryPage;
}());

//# sourceMappingURL=history.js.map

/***/ }),

/***/ 110:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return ProfilePage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(9);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__home_home__ = __webpack_require__(83);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__providers_rest_rest__ = __webpack_require__(13);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};





/**
 * Generated class for the ProfilePage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */
var ProfilePage = (function () {
    function ProfilePage(alertCtrl, navCtrl, navParams, restProvider, app, toastCtrl) {
        this.alertCtrl = alertCtrl;
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.restProvider = restProvider;
        this.app = app;
        this.toastCtrl = toastCtrl;
        this.password_data = { con_password: '', password: '' };
        this.phone_data = { phone: '' };
    }
    ProfilePage.prototype.ionViewDidLoad = function () {
        console.log('ionViewDidLoad ProfilePage');
    };
    ProfilePage.prototype.logout = function () {
        var _this = this;
        var confirm = this.alertCtrl.create({
            title: 'Are you sure?',
            message: 'Do you want to Logout.',
            buttons: [
                {
                    text: 'Cancel',
                    handler: function () {
                        console.log('Disagree clicked');
                    }
                },
                {
                    text: 'Logout',
                    handler: function () {
                        _this.restProvider.setChatStatus(0)
                            .then(function (result) {
                            _this.restProvider.logout();
                            _this.app.getRootNav().setRoot(__WEBPACK_IMPORTED_MODULE_2__home_home__["a" /* HomePage */]);
                        }, function (err) {
                            console.log(err);
                        });
                    }
                }
            ]
        });
        confirm.present();
    };
    ProfilePage.prototype.changepwd = function () {
        var _this = this;
        if (this.password_data.password != this.password_data.con_password) {
            var confirm_1 = this.alertCtrl.create({
                title: 'Password Mismatch',
                message: 'Password didnot match with confirm password',
                buttons: [
                    {
                        text: 'OK',
                        handler: function () {
                            //do nothing
                        }
                    }
                ]
            });
            confirm_1.present();
            return;
        }
        if (this.password_data.password == '' || this.password_data.con_password == '') {
            var confirm_2 = this.alertCtrl.create({
                title: 'Enter Password',
                message: 'Please enter password',
                buttons: [
                    {
                        text: 'OK',
                        handler: function () {
                            //do nothing
                        }
                    }
                ]
            });
            confirm_2.present();
            return;
        }
        this.restProvider.changePassword(this.password_data).then(function (result) {
            if (result['error']) {
                _this.presentToast(result['error']);
            }
            else {
                var confirm_3 = _this.alertCtrl.create({
                    title: 'Password Changed',
                    message: result['msg'],
                    buttons: [
                        {
                            text: 'OK',
                            handler: function () {
                                _this.restProvider.logout();
                                _this.app.getRootNav().setRoot(__WEBPACK_IMPORTED_MODULE_2__home_home__["a" /* HomePage */]);
                            }
                        }
                    ]
                });
                confirm_3.present();
            }
        }, function (err) {
            console.log(err);
        });
    };
    ProfilePage.prototype.presentToast = function (msg) {
        var toast = this.toastCtrl.create({
            message: msg,
            duration: 5000,
            position: 'top'
        });
        toast.present();
    };
    ProfilePage.prototype.changePhone = function () {
        var _this = this;
        if (this.phone_data.phone == '') {
            var confirm_4 = this.alertCtrl.create({
                title: 'Enter Phone',
                message: 'Please enter phone number',
                buttons: [
                    {
                        text: 'OK',
                        handler: function () {
                            //do nothing
                        }
                    }
                ]
            });
            confirm_4.present();
            return;
        }
        this.restProvider.changePhone(this.phone_data.phone).then(function (result) {
            if (result['error']) {
                _this.presentToast(result['error']);
            }
            else {
                _this.presentToast(result['msg']);
                _this.phone_data.phone = '';
            }
        }, function (err) {
            console.log(err);
        });
    };
    ProfilePage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'page-profile',template:/*ion-inline-start:"/Users/admin/Desktop/kitclient/src/pages/profile/profile.html"*/'<ion-content align="center" padding>\n        <ion-img class="Logo" align="center" src="assets/kitvision.png"></ion-img>\n        <hr class="logohr">\n        <h2><ion-label align="center" TextDecorations="Underline">PROFILE</ion-label>\n        </h2><hr class="logohr">\n\n<br/><div>\n       <!-- <ion-img align="center" class="profilePic" src="assets/avatar.png"></ion-img>\n        <ion-icon class="pen" ios="ios-create" md="md-create"></ion-icon>\n       \n<br/>\n      <hr class="logohr"> -->\n      <ion-label>CHANGE PASSWORD</ion-label>\n      <ion-input class="input" type="password" placeholder="Enter Password" [(ngModel)]="password_data.password"></ion-input>\n      <ion-input class="input" type="password" placeholder="Confirm Password" [(ngModel)]="password_data.con_password"></ion-input>\n      <button  round class="buttonLogout" (click)="changepwd()">RESET PASSWORD</button>\n</div><br/>\n<hr class="logohr">\n      <ion-label>MODIFY PHONENUMBER</ion-label>\n      <ion-input type="number" class="input" placeholder="Modify PhoneNumber" [(ngModel)]="phone_data.phone"></ion-input>\n      <button  round class="buttonLogout" (click)="changePhone()">UPDATE NUMBER</button>\n      <br><br>\n      <hr class="logohr">\n      <ion-label>LOGOUT</ion-label>\n      <button  round class="buttonLogout" align="center" (click)="logout()">LOGOUT</button>\n\n</ion-content>\n'/*ion-inline-end:"/Users/admin/Desktop/kitclient/src/pages/profile/profile.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["a" /* AlertController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["h" /* NavController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["i" /* NavParams */], __WEBPACK_IMPORTED_MODULE_3__providers_rest_rest__["a" /* RestProvider */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["b" /* App */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["k" /* ToastController */]])
    ], ProfilePage);
    return ProfilePage;
}());

//# sourceMappingURL=profile.js.map

/***/ }),

/***/ 111:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return ResetpasswordPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(9);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__providers_rest_rest__ = __webpack_require__(13);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};




/**
 * Generated class for the ResetpasswordPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */
var ResetpasswordPage = (function () {
    function ResetpasswordPage(alertCtrl, navCtrl, navParams, restProvider, toastCtrl) {
        this.alertCtrl = alertCtrl;
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.restProvider = restProvider;
        this.toastCtrl = toastCtrl;
        this.credential = { email: '' };
    }
    ResetpasswordPage.prototype.ionViewDidLoad = function () {
        console.log('ionViewDidLoad ResetpasswordPage');
    };
    ResetpasswordPage.prototype.resetEmail = function () {
        var _this = this;
        this.restProvider.resetpwd(this.credential.email).then(function (result) {
            console.log(result);
            var alert = _this.alertCtrl.create({
                title: 'DONE',
                message: result['msg'],
                buttons: [
                    {
                        text: 'OK',
                        role: 'OK',
                        handler: function () {
                            console.log('Ok clicked');
                        }
                    }
                ]
            });
            alert.present();
        }, function (err) {
            console.log(err);
        });
    };
    ResetpasswordPage.prototype.presentToast = function (msg) {
        var toast = this.toastCtrl.create({
            message: msg,
            duration: 2000
        });
        toast.present();
    };
    ResetpasswordPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'page-resetpassword',template:/*ion-inline-start:"/Users/admin/Desktop/kitclient/src/pages/resetpassword/resetpassword.html"*/'<!--\n  Generated template for the ResetpasswordPage page.\n\n  See http://ionicframework.com/docs/components/#navigation for more info on\n  Ionic pages and navigation.\n-->\n<ion-header>\n\n  <ion-navbar>\n    <ion-title>resetpassword</ion-title>\n  </ion-navbar>\n\n</ion-header>\n\n\n<ion-content align="center" padding>\n\n  <br><br><br><br><br><br><br><br><br>\n      <ion-img  align="center" class="Logo" src="assets/kitvision.png"></ion-img>\n      <br><br><br><br>\n    \n        <ion-input class="input" placeholder="Enter Your Email" [(ngModel)]="credential.email"></ion-input>\n        <br/>\n<button  round class="btnSubmit" align="center" (click) = "resetEmail()">SUBMIT</button>\n\n\n</ion-content>\n'/*ion-inline-end:"/Users/admin/Desktop/kitclient/src/pages/resetpassword/resetpassword.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["a" /* AlertController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["h" /* NavController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["i" /* NavParams */], __WEBPACK_IMPORTED_MODULE_2__providers_rest_rest__["a" /* RestProvider */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["k" /* ToastController */]])
    ], ResetpasswordPage);
    return ResetpasswordPage;
}());

//# sourceMappingURL=resetpassword.js.map

/***/ }),

/***/ 125:
/***/ (function(module, exports) {

function webpackEmptyAsyncContext(req) {
	// Here Promise.resolve().then() is used instead of new Promise() to prevent
	// uncatched exception popping up in devtools
	return Promise.resolve().then(function() {
		throw new Error("Cannot find module '" + req + "'.");
	});
}
webpackEmptyAsyncContext.keys = function() { return []; };
webpackEmptyAsyncContext.resolve = webpackEmptyAsyncContext;
module.exports = webpackEmptyAsyncContext;
webpackEmptyAsyncContext.id = 125;

/***/ }),

/***/ 13:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return RestProvider; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_common_http__ = __webpack_require__(168);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_core__ = __webpack_require__(0);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


/*
  Generated class for the RestProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/
var RestProvider = (function () {
    function RestProvider(http) {
        this.http = http;
        this.apiUrl = 'http://app.kitvision.org/api';
        this.thread_id = 'none';
        console.log('Hello RestProvider Provider');
    }
    RestProvider.prototype.login = function (data) {
        var _this = this;
        this.username = data.username;
        return new Promise(function (resolve, reject) {
            _this.http.post(_this.apiUrl + '/login', JSON.stringify(data))
                .subscribe(function (res) {
                // login successful if there's a jwt token in the response
                var token = res['token'];
                if (token) {
                    // set token property
                    _this.token = token;
                    // store username and jwt token in local storage to keep user logged in between page refreshes
                    localStorage.setItem('currentUser', JSON.stringify({ "username": _this.username, "token": _this.token }));
                    // return true to indicate successful login
                    resolve(res);
                }
                else {
                    // return false to indicate failed login
                    resolve(res);
                }
                //console.log(res['token']);
                resolve(res);
            }, function (err) {
                reject(err);
            });
        });
    };
    RestProvider.prototype.logout = function () {
        // clear token remove user from local storage to log user out
        this.token = null;
        localStorage.removeItem('currentUser');
    };
    RestProvider.prototype.resetpwd = function (email) {
        var _this = this;
        var body = new __WEBPACK_IMPORTED_MODULE_0__angular_common_http__["c" /* HttpParams */]()
            .set('email', email);
        return new Promise(function (resolve, reject) {
            _this.http.post(_this.apiUrl + '/reset', body.toString(), {
                headers: { 'Content-Type': 'application/x-www-form-urlencoded' },
            })
                .subscribe(function (res) {
                resolve(res);
            }, function (err) {
                reject(err);
            });
        });
    };
    RestProvider.prototype.getScreens = function (token) {
        var _this = this;
        //console.log(token);
        var body = new __WEBPACK_IMPORTED_MODULE_0__angular_common_http__["c" /* HttpParams */]()
            .set('api_token', token);
        return new Promise(function (resolve, reject) {
            _this.http.post(_this.apiUrl + '/screens/get', body.toString(), {
                headers: { 'Content-Type': 'application/x-www-form-urlencoded' },
            })
                .subscribe(function (res) {
                resolve(res);
            }, function (err) {
                reject(err);
            });
        });
    };
    RestProvider.prototype.addTicket = function (ticket) {
        var _this = this;
        console.log(ticket);
        var body = new __WEBPACK_IMPORTED_MODULE_0__angular_common_http__["c" /* HttpParams */]()
            .set('api_token', ticket.api_token)
            .set('screen_id', ticket.screen_id)
            .set('prob_desc', ticket.prob_desc);
        return new Promise(function (resolve, reject) {
            _this.http.post(_this.apiUrl + '/ticket/add', body.toString(), {
                headers: { 'Content-Type': 'application/x-www-form-urlencoded' },
            })
                .subscribe(function (res) {
                resolve(res);
            }, function (err) {
                reject(err);
            });
        });
    };
    RestProvider.prototype.getFiles = function (token) {
        var _this = this;
        //console.log(token);
        var body = new __WEBPACK_IMPORTED_MODULE_0__angular_common_http__["c" /* HttpParams */]()
            .set('api_token', token);
        return new Promise(function (resolve, reject) {
            _this.http.post(_this.apiUrl + '/files/troubleshoot', body.toString(), {
                headers: { 'Content-Type': 'application/x-www-form-urlencoded' },
            })
                .subscribe(function (res) {
                //console.log(res);
                resolve(res);
            }, function (err) {
                reject(err);
            });
        });
    };
    RestProvider.prototype.getHistory = function (token) {
        var _this = this;
        //console.log(token);
        var body = new __WEBPACK_IMPORTED_MODULE_0__angular_common_http__["c" /* HttpParams */]()
            .set('api_token', token);
        return new Promise(function (resolve, reject) {
            _this.http.post(_this.apiUrl + '/ticket/client/all', body.toString(), {
                headers: { 'Content-Type': 'application/x-www-form-urlencoded' },
            })
                .subscribe(function (res) {
                console.log(res);
                resolve(res);
            }, function (err) {
                reject(err);
            });
        });
    };
    RestProvider.prototype.changePassword = function (data) {
        var _this = this;
        var body = new __WEBPACK_IMPORTED_MODULE_0__angular_common_http__["c" /* HttpParams */]()
            .set('password', data.password)
            .set('api_token', this.token)
            .set('con_password', data.con_password);
        return new Promise(function (resolve, reject) {
            _this.http.post(_this.apiUrl + '/user/changepwd', body.toString(), {
                headers: { 'Content-Type': 'application/x-www-form-urlencoded' },
            })
                .subscribe(function (res) {
                resolve(res);
            }, function (err) {
                reject(err);
            });
        });
    };
    RestProvider.prototype.changePhone = function (phone) {
        var _this = this;
        var body = new __WEBPACK_IMPORTED_MODULE_0__angular_common_http__["c" /* HttpParams */]()
            .set('api_token', this.token)
            .set('phone', phone);
        return new Promise(function (resolve, reject) {
            _this.http.post(_this.apiUrl + '/user/changePhone', body.toString(), {
                headers: { 'Content-Type': 'application/x-www-form-urlencoded' },
            })
                .subscribe(function (res) {
                resolve(res);
            }, function (err) {
                reject(err);
            });
        });
    };
    RestProvider.prototype.getChatStatus = function () {
        var _this = this;
        var body = new __WEBPACK_IMPORTED_MODULE_0__angular_common_http__["c" /* HttpParams */]()
            .set('api_token', this.token);
        return new Promise(function (resolve, reject) {
            _this.http.post(_this.apiUrl + '/chat/status/get', body.toString(), {
                headers: { 'Content-Type': 'application/x-www-form-urlencoded' },
            })
                .subscribe(function (res) {
                resolve(res);
            }, function (err) {
                reject(err);
            });
        });
    };
    RestProvider.prototype.setChatStatus = function (status) {
        var _this = this;
        if (this.token == null)
            return;
        var body = new __WEBPACK_IMPORTED_MODULE_0__angular_common_http__["c" /* HttpParams */]()
            .set('client', '1')
            .set('status', status)
            .set('api_token', this.token);
        return new Promise(function (resolve, reject) {
            _this.http.post(_this.apiUrl + '/chat/status/set', body.toString(), {
                headers: { 'Content-Type': 'application/x-www-form-urlencoded' },
            })
                .subscribe(function (res) {
                resolve(res);
            }, function (err) {
                reject(err);
            });
        });
    };
    RestProvider.prototype.getMessages = function () {
        var _this = this;
        var body = new __WEBPACK_IMPORTED_MODULE_0__angular_common_http__["c" /* HttpParams */]()
            .set('thread_id', this.thread_id)
            .set('api_token', this.token);
        return new Promise(function (resolve, reject) {
            _this.http.post(_this.apiUrl + '/chat/client/get', body.toString(), {
                headers: { 'Content-Type': 'application/x-www-form-urlencoded' },
            })
                .subscribe(function (res) {
                resolve(res);
            }, function (err) {
                reject(err);
            });
        });
    };
    RestProvider.prototype.getTracking = function (user_id) {
        var _this = this;
        console.log(user_id);
        var body = new __WEBPACK_IMPORTED_MODULE_0__angular_common_http__["c" /* HttpParams */]()
            .set('user_id', user_id)
            .set('api_token', this.token);
        return new Promise(function (resolve, reject) {
            _this.http.post(_this.apiUrl + '/location/get', body.toString(), {
                headers: { 'Content-Type': 'application/x-www-form-urlencoded' },
            })
                .subscribe(function (res) {
                resolve(res);
            }, function (err) {
                reject(err);
            });
        });
    };
    RestProvider.prototype.sendMessage = function (message) {
        var _this = this;
        var body = new __WEBPACK_IMPORTED_MODULE_0__angular_common_http__["c" /* HttpParams */]()
            .set('message', message)
            .set('thread_id', this.thread_id)
            .set('api_token', this.token);
        return new Promise(function (resolve, reject) {
            _this.http.post(_this.apiUrl + '/chat/client/sendMessage', body.toString(), {
                headers: { 'Content-Type': 'application/x-www-form-urlencoded' },
            })
                .subscribe(function (res) {
                resolve(res);
            }, function (err) {
                reject(err);
            });
        });
    };
    RestProvider.prototype.getTicketDetails = function (ticket_id, lat, long) {
        var _this = this;
        var body = new __WEBPACK_IMPORTED_MODULE_0__angular_common_http__["c" /* HttpParams */]()
            .set('ticket_id', ticket_id)
            .set('latitude', lat)
            .set('longitude', long)
            .set('api_token', this.token);
        return new Promise(function (resolve, reject) {
            _this.http.post(_this.apiUrl + '/ticket/getTicket', body.toString(), {
                headers: { 'Content-Type': 'application/x-www-form-urlencoded' },
            })
                .subscribe(function (res) {
                resolve(res);
            }, function (err) {
                reject(err);
            });
        });
    };
    RestProvider.prototype.declineTicket = function (ticket_id) {
        var _this = this;
        var body = new __WEBPACK_IMPORTED_MODULE_0__angular_common_http__["c" /* HttpParams */]()
            .set('ticket_id', ticket_id)
            .set('api_token', this.token);
        return new Promise(function (resolve, reject) {
            _this.http.post(_this.apiUrl + '/ticket/client/decline', body.toString(), {
                headers: { 'Content-Type': 'application/x-www-form-urlencoded' },
            })
                .subscribe(function (res) {
                resolve(res);
            }, function (err) {
                reject(err);
            });
        });
    };
    RestProvider.prototype.getTimeSinceArrival = function (ticket_id) {
        var _this = this;
        var body = new __WEBPACK_IMPORTED_MODULE_0__angular_common_http__["c" /* HttpParams */]()
            .set('ticket_id', ticket_id)
            .set('api_token', this.token);
        return new Promise(function (resolve, reject) {
            _this.http.post(_this.apiUrl + '/ticket/timelog', body.toString(), {
                headers: { 'Content-Type': 'application/x-www-form-urlencoded' },
            })
                .subscribe(function (res) {
                resolve(res);
            }, function (err) {
                reject(err);
            });
        });
    };
    RestProvider.prototype.saveFeedBack = function (ticket_id, feedback, rating) {
        var _this = this;
        var body = new __WEBPACK_IMPORTED_MODULE_0__angular_common_http__["c" /* HttpParams */]()
            .set('ticket_id', ticket_id)
            .set('feedback', feedback)
            .set('rating', rating)
            .set('api_token', this.token);
        return new Promise(function (resolve, reject) {
            _this.http.post(_this.apiUrl + '/ticket/feedback', body.toString(), {
                headers: { 'Content-Type': 'application/x-www-form-urlencoded' },
            })
                .subscribe(function (res) {
                resolve(res);
            }, function (err) {
                reject(err);
            });
        });
    };
    RestProvider.prototype.setChatToken = function (token_id) {
        var _this = this;
        var body = new __WEBPACK_IMPORTED_MODULE_0__angular_common_http__["c" /* HttpParams */]()
            .set('chat_token', token_id)
            .set('api_token', this.token);
        return new Promise(function (resolve, reject) {
            _this.http.post(_this.apiUrl + '/chat/setToken', body.toString(), {
                headers: { 'Content-Type': 'application/x-www-form-urlencoded' },
            })
                .subscribe(function (res) {
                resolve(res);
            }, function (err) {
                reject(err);
            });
        });
    };
    RestProvider.prototype.getThread = function () {
        return this.thread_id;
    };
    RestProvider.prototype.closeThread = function () {
        var _this = this;
        var body = new __WEBPACK_IMPORTED_MODULE_0__angular_common_http__["c" /* HttpParams */]()
            .set('thread_id', this.thread_id)
            .set('api_token', this.token);
        return new Promise(function (resolve, reject) {
            _this.http.post(_this.apiUrl + '/chat/close', body.toString(), {
                headers: { 'Content-Type': 'application/x-www-form-urlencoded' },
            })
                .subscribe(function (res) {
                resolve(res);
            }, function (err) {
                reject(err);
            });
        });
    };
    RestProvider = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_1__angular_core__["A" /* Injectable */])(),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_0__angular_common_http__["a" /* HttpClient */]])
    ], RestProvider);
    return RestProvider;
}());

//# sourceMappingURL=rest.js.map

/***/ }),

/***/ 167:
/***/ (function(module, exports, __webpack_require__) {

var map = {
	"../pages/chat/chat.module": [
		296,
		12
	],
	"../pages/dashboard/dashboard.module": [
		297,
		11
	],
	"../pages/downloads/downloads.module": [
		298,
		10
	],
	"../pages/feedback/feedback.module": [
		299,
		9
	],
	"../pages/history/history.module": [
		300,
		8
	],
	"../pages/login/login.module": [
		301,
		7
	],
	"../pages/offline/offline.module": [
		302,
		6
	],
	"../pages/profile/profile.module": [
		303,
		5
	],
	"../pages/resetpassword/resetpassword.module": [
		304,
		4
	],
	"../pages/support-tracker/support-tracker.module": [
		305,
		3
	],
	"../pages/support/support.module": [
		306,
		2
	],
	"../pages/trb-shoot/trb-shoot.module": [
		308,
		1
	],
	"../pages/verify-support/verify-support.module": [
		307,
		0
	]
};
function webpackAsyncContext(req) {
	var ids = map[req];
	if(!ids)
		return Promise.reject(new Error("Cannot find module '" + req + "'."));
	return __webpack_require__.e(ids[1]).then(function() {
		return __webpack_require__(ids[0]);
	});
};
webpackAsyncContext.keys = function webpackAsyncContextKeys() {
	return Object.keys(map);
};
webpackAsyncContext.id = 167;
module.exports = webpackAsyncContext;

/***/ }),

/***/ 215:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return DownloadsPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(9);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


/**
 * Generated class for the DownloadsPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */
var DownloadsPage = (function () {
    function DownloadsPage(navCtrl, navParams) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
    }
    DownloadsPage.prototype.ionViewDidLoad = function () {
        console.log('ionViewDidLoad DownloadsPage');
    };
    DownloadsPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'page-downloads',template:/*ion-inline-start:"/Users/admin/Desktop/kitclient/src/pages/downloads/downloads.html"*/'<ion-content align="center" padding>\n    <ion-img class="Logo" align="center" src="assets/kitvision.png"></ion-img>\n    <hr class="logohr">\n    <h2><ion-label align="center" TextDecorations="Underline">CHAT</ion-label>\n    </h2><hr class="logohr">\n    </ion-content>\n    '/*ion-inline-end:"/Users/admin/Desktop/kitclient/src/pages/downloads/downloads.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["h" /* NavController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["i" /* NavParams */]])
    ], DownloadsPage);
    return DownloadsPage;
}());

//# sourceMappingURL=downloads.js.map

/***/ }),

/***/ 216:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return LoginPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(9);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


/**
 * Generated class for the LoginPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */
var LoginPage = (function () {
    function LoginPage(navCtrl, navParams) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
    }
    LoginPage.prototype.ionViewDidLoad = function () {
        console.log('ionViewDidLoad LoginPage');
    };
    LoginPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'page-login',template:/*ion-inline-start:"/Users/admin/Desktop/kitclient/src/pages/login/login.html"*/'<!--\n  Generated template for the LoginPage page.\n\n  See http://ionicframework.com/docs/components/#navigation for more info on\n  Ionic pages and navigation.\n-->\n<ion-header>\n\n  <ion-navbar>\n    <ion-title>Login Page</ion-title>\n  </ion-navbar>\n\n</ion-header>\n\n\n<ion-content padding>\n<p>This is Login Page!</p>\n</ion-content>\n'/*ion-inline-end:"/Users/admin/Desktop/kitclient/src/pages/login/login.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["h" /* NavController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["i" /* NavParams */]])
    ], LoginPage);
    return LoginPage;
}());

//# sourceMappingURL=login.js.map

/***/ }),

/***/ 217:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return OfflinePage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(9);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


/**
 * Generated class for the OfflinePage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */
var OfflinePage = (function () {
    function OfflinePage(navCtrl, navParams) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
    }
    OfflinePage.prototype.ionViewDidLoad = function () {
        console.log('ionViewDidLoad OfflinePage');
    };
    OfflinePage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'page-offline',template:/*ion-inline-start:"/Users/admin/Desktop/kitclient/src/pages/offline/offline.html"*/'<ion-header>\n\n    <ion-navbar>\n      <ion-title>CHAT</ion-title>\n    </ion-navbar>\n  </ion-header>\n<ion-content align="center" padding>\n  <ion-img class="Logo" align="center" src="assets/kitvision.png"></ion-img>\n  <hr class="logohr">\n  <h2><ion-label align="center" TextDecorations="Underline">CHAT</ion-label>\n  </h2><hr class="logohr">\n\n  <br><br>\n  <ion-img class="offlineChat"  src="assets/offlinechat.png"></ion-img>\n  <br><br>\n  <ion-label>NO ONE IS ONLINE</ion-label>\n  <ion-label>PLEASE SEND EMAIL ON</ion-label>\n  <ion-label ><h3><u>md@kitvision.net</u></h3></ion-label>\n</ion-content>\n'/*ion-inline-end:"/Users/admin/Desktop/kitclient/src/pages/offline/offline.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["h" /* NavController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["i" /* NavParams */]])
    ], OfflinePage);
    return OfflinePage;
}());

//# sourceMappingURL=offline.js.map

/***/ }),

/***/ 218:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_platform_browser_dynamic__ = __webpack_require__(219);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__app_module__ = __webpack_require__(239);


Object(__WEBPACK_IMPORTED_MODULE_0__angular_platform_browser_dynamic__["a" /* platformBrowserDynamic */])().bootstrapModule(__WEBPACK_IMPORTED_MODULE_1__app_module__["a" /* AppModule */]);
//# sourceMappingURL=main.js.map

/***/ }),

/***/ 239:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return AppModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_platform_browser__ = __webpack_require__(31);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_ionic_angular__ = __webpack_require__(9);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__ionic_native_splash_screen__ = __webpack_require__(212);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__ionic_native_status_bar__ = __webpack_require__(213);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__ionic_native_geolocation__ = __webpack_require__(293);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__ionic_native_push__ = __webpack_require__(172);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7__ionic_native_google_maps__ = __webpack_require__(169);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_8__ionic_native_network__ = __webpack_require__(214);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_9__app_component__ = __webpack_require__(295);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_10__pages_home_home__ = __webpack_require__(83);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_11__pages_dashboard_dashboard__ = __webpack_require__(106);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_12__pages_trb_shoot_trb_shoot__ = __webpack_require__(107);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_13__pages_chat_chat__ = __webpack_require__(105);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_14__pages_support_support__ = __webpack_require__(108);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_15__pages_history_history__ = __webpack_require__(109);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_16__pages_profile_profile__ = __webpack_require__(110);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_17__pages_login_login__ = __webpack_require__(216);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_18__pages_verify_support_verify_support__ = __webpack_require__(52);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_19__pages_support_tracker_support_tracker__ = __webpack_require__(53);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_20__pages_offline_offline__ = __webpack_require__(217);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_21__pages_downloads_downloads__ = __webpack_require__(215);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_22__providers_rest_rest__ = __webpack_require__(13);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_23__angular_common_http__ = __webpack_require__(168);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_24__pages_resetpassword_resetpassword__ = __webpack_require__(111);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_25__pages_feedback_feedback__ = __webpack_require__(54);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};


























var AppModule = (function () {
    function AppModule() {
    }
    AppModule = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_1__angular_core__["I" /* NgModule */])({
            declarations: [
                __WEBPACK_IMPORTED_MODULE_9__app_component__["a" /* MyApp */],
                __WEBPACK_IMPORTED_MODULE_10__pages_home_home__["a" /* HomePage */],
                __WEBPACK_IMPORTED_MODULE_11__pages_dashboard_dashboard__["a" /* DashboardPage */],
                __WEBPACK_IMPORTED_MODULE_12__pages_trb_shoot_trb_shoot__["a" /* TrbShootPage */],
                __WEBPACK_IMPORTED_MODULE_13__pages_chat_chat__["a" /* ChatPage */],
                __WEBPACK_IMPORTED_MODULE_14__pages_support_support__["a" /* SupportPage */],
                __WEBPACK_IMPORTED_MODULE_15__pages_history_history__["a" /* HistoryPage */],
                __WEBPACK_IMPORTED_MODULE_16__pages_profile_profile__["a" /* ProfilePage */],
                __WEBPACK_IMPORTED_MODULE_17__pages_login_login__["a" /* LoginPage */],
                __WEBPACK_IMPORTED_MODULE_18__pages_verify_support_verify_support__["a" /* VerifySupportPage */],
                __WEBPACK_IMPORTED_MODULE_19__pages_support_tracker_support_tracker__["a" /* SupportTrackerPage */],
                __WEBPACK_IMPORTED_MODULE_20__pages_offline_offline__["a" /* OfflinePage */],
                __WEBPACK_IMPORTED_MODULE_24__pages_resetpassword_resetpassword__["a" /* ResetpasswordPage */],
                __WEBPACK_IMPORTED_MODULE_21__pages_downloads_downloads__["a" /* DownloadsPage */],
                __WEBPACK_IMPORTED_MODULE_25__pages_feedback_feedback__["a" /* FeedbackPage */],
            ],
            imports: [
                __WEBPACK_IMPORTED_MODULE_0__angular_platform_browser__["a" /* BrowserModule */],
                __WEBPACK_IMPORTED_MODULE_23__angular_common_http__["b" /* HttpClientModule */],
                __WEBPACK_IMPORTED_MODULE_2_ionic_angular__["e" /* IonicModule */].forRoot(__WEBPACK_IMPORTED_MODULE_9__app_component__["a" /* MyApp */], {}, {
                    links: [
                        { loadChildren: '../pages/chat/chat.module#ChatPageModule', name: 'ChatPage', segment: 'chat', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/dashboard/dashboard.module#DashboardPageModule', name: 'DashboardPage', segment: 'dashboard', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/downloads/downloads.module#DownloadsPageModule', name: 'DownloadsPage', segment: 'downloads', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/feedback/feedback.module#FeedbackPageModule', name: 'FeedbackPage', segment: 'feedback', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/history/history.module#HistoryPageModule', name: 'HistoryPage', segment: 'history', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/login/login.module#LoginPageModule', name: 'LoginPage', segment: 'login', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/offline/offline.module#OfflinePageModule', name: 'OfflinePage', segment: 'offline', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/profile/profile.module#ProfilePageModule', name: 'ProfilePage', segment: 'profile', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/resetpassword/resetpassword.module#ResetpasswordPageModule', name: 'ResetpasswordPage', segment: 'resetpassword', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/support-tracker/support-tracker.module#SupportTrackerPageModule', name: 'SupportTrackerPage', segment: 'support-tracker', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/support/support.module#SupportPageModule', name: 'SupportPage', segment: 'support', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/verify-support/verify-support.module#VerifySupportPageModule', name: 'VerifySupportPage', segment: 'verify-support', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/trb-shoot/trb-shoot.module#TrbShootPageModule', name: 'TrbShootPage', segment: 'trb-shoot', priority: 'low', defaultHistory: [] }
                    ]
                })
            ],
            bootstrap: [__WEBPACK_IMPORTED_MODULE_2_ionic_angular__["c" /* IonicApp */]],
            entryComponents: [
                __WEBPACK_IMPORTED_MODULE_9__app_component__["a" /* MyApp */],
                __WEBPACK_IMPORTED_MODULE_10__pages_home_home__["a" /* HomePage */],
                __WEBPACK_IMPORTED_MODULE_11__pages_dashboard_dashboard__["a" /* DashboardPage */],
                __WEBPACK_IMPORTED_MODULE_12__pages_trb_shoot_trb_shoot__["a" /* TrbShootPage */],
                __WEBPACK_IMPORTED_MODULE_13__pages_chat_chat__["a" /* ChatPage */],
                __WEBPACK_IMPORTED_MODULE_14__pages_support_support__["a" /* SupportPage */],
                __WEBPACK_IMPORTED_MODULE_15__pages_history_history__["a" /* HistoryPage */],
                __WEBPACK_IMPORTED_MODULE_16__pages_profile_profile__["a" /* ProfilePage */],
                __WEBPACK_IMPORTED_MODULE_17__pages_login_login__["a" /* LoginPage */],
                __WEBPACK_IMPORTED_MODULE_18__pages_verify_support_verify_support__["a" /* VerifySupportPage */],
                __WEBPACK_IMPORTED_MODULE_19__pages_support_tracker_support_tracker__["a" /* SupportTrackerPage */],
                __WEBPACK_IMPORTED_MODULE_20__pages_offline_offline__["a" /* OfflinePage */],
                __WEBPACK_IMPORTED_MODULE_21__pages_downloads_downloads__["a" /* DownloadsPage */],
                __WEBPACK_IMPORTED_MODULE_24__pages_resetpassword_resetpassword__["a" /* ResetpasswordPage */],
                __WEBPACK_IMPORTED_MODULE_25__pages_feedback_feedback__["a" /* FeedbackPage */],
            ],
            providers: [
                __WEBPACK_IMPORTED_MODULE_4__ionic_native_status_bar__["a" /* StatusBar */],
                __WEBPACK_IMPORTED_MODULE_3__ionic_native_splash_screen__["a" /* SplashScreen */],
                __WEBPACK_IMPORTED_MODULE_5__ionic_native_geolocation__["a" /* Geolocation */],
                __WEBPACK_IMPORTED_MODULE_7__ionic_native_google_maps__["a" /* GoogleMaps */],
                __WEBPACK_IMPORTED_MODULE_6__ionic_native_push__["a" /* Push */],
                { provide: __WEBPACK_IMPORTED_MODULE_1__angular_core__["u" /* ErrorHandler */], useClass: __WEBPACK_IMPORTED_MODULE_2_ionic_angular__["d" /* IonicErrorHandler */] },
                __WEBPACK_IMPORTED_MODULE_22__providers_rest_rest__["a" /* RestProvider */],
                __WEBPACK_IMPORTED_MODULE_8__ionic_native_network__["a" /* Network */],
            ]
        })
    ], AppModule);
    return AppModule;
}());

//# sourceMappingURL=app.module.js.map

/***/ }),

/***/ 295:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return MyApp; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(9);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__ionic_native_status_bar__ = __webpack_require__(213);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__ionic_native_splash_screen__ = __webpack_require__(212);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__ionic_native_network__ = __webpack_require__(214);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__pages_home_home__ = __webpack_require__(83);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};







var MyApp = (function () {
    function MyApp(platform, statusBar, splashScreen, network, alertCtrl) {
        var _this = this;
        this.rootPage = __WEBPACK_IMPORTED_MODULE_5__pages_home_home__["a" /* HomePage */];
        platform.ready().then(function () {
            _this.rootPage = __WEBPACK_IMPORTED_MODULE_5__pages_home_home__["a" /* HomePage */];
            statusBar.styleDefault();
            splashScreen.hide();
            var disconnectSubscription = network.onDisconnect().subscribe(function () {
                var alert = alertCtrl.create({
                    title: 'Offline',
                    subTitle: 'No Internet Connection is available the app will not work',
                    buttons: ['Ok']
                });
                alert.present();
            });
        });
    }
    MyApp = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({template:/*ion-inline-start:"/Users/admin/Desktop/kitclient/src/app/app.html"*/'<ion-nav [root]="rootPage"></ion-nav>\n'/*ion-inline-end:"/Users/admin/Desktop/kitclient/src/app/app.html"*/
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["j" /* Platform */], __WEBPACK_IMPORTED_MODULE_2__ionic_native_status_bar__["a" /* StatusBar */], __WEBPACK_IMPORTED_MODULE_3__ionic_native_splash_screen__["a" /* SplashScreen */], __WEBPACK_IMPORTED_MODULE_4__ionic_native_network__["a" /* Network */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["a" /* AlertController */]])
    ], MyApp);
    return MyApp;
}());

//# sourceMappingURL=app.component.js.map

/***/ }),

/***/ 52:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return VerifySupportPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(9);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__support_tracker_support_tracker__ = __webpack_require__(53);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__providers_rest_rest__ = __webpack_require__(13);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__ionic_native_google_maps__ = __webpack_require__(169);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};





/**
 * Generated class for the VerifySupportPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */
var VerifySupportPage = (function () {
    function VerifySupportPage(navCtrl, navParams, restProvider, toastCtrl, loadingCtrl, alertCtrl) {
        var _this = this;
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.restProvider = restProvider;
        this.toastCtrl = toastCtrl;
        this.loadingCtrl = loadingCtrl;
        this.alertCtrl = alertCtrl;
        //this.getData();
        this.loading = this.loadingCtrl.create({
            content: 'Please wait...'
        });
        this.loading.present();
        console.log("Alert");
        this.restProvider.getTracking(this.navParams.get('tech_id')).then(function (result) {
            _this.latitude = result['latitude'];
            _this.longitude = result['longitude'];
            _this.loadMap();
        });
    }
    VerifySupportPage.prototype.getData = function () {
        var _this = this;
        this.loading = this.loadingCtrl.create({
            content: 'Please wait...'
        });
        this.loading.present();
        this.restProvider.getTracking(this.navParams.get('tech_id')).then(function (result) {
            _this.latitude = result['latitude'];
            _this.longitude = result['longitude'];
            _this.loadMap();
        });
    };
    VerifySupportPage.prototype.ionViewDidLoad = function () {
        console.log('ionViewDidLoad VerifySupportPage');
    };
    VerifySupportPage.prototype.supportTracker = function () {
        this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_2__support_tracker_support_tracker__["a" /* SupportTrackerPage */]);
    };
    VerifySupportPage.prototype.loadMap = function () {
        var _this = this;
        var mapOptions = {
            camera: {
                target: {
                    lat: this.latitude,
                    lng: this.longitude,
                },
                zoom: 12,
                tilt: 30
            }
        };
        var element = document.getElementById('map');
        this.map = __WEBPACK_IMPORTED_MODULE_4__ionic_native_google_maps__["a" /* GoogleMaps */].create(element, mapOptions);
        // this.map = GoogleMaps.create('map', mapOptions);
        // Wait the MAP_READY before using any methods.
        this.map.one(__WEBPACK_IMPORTED_MODULE_4__ionic_native_google_maps__["b" /* GoogleMapsEvent */].MAP_READY)
            .then(function () {
            _this.restProvider.getTicketDetails(_this.navParams.get('ticket_id'), _this.latitude, _this.longitude).then(function (result) {
                _this.ticket_id = result['ticket'].ticket_id;
                _this.address = result['ticket'].address;
                _this.avatar = result['ticket'].avatar;
                _this.assigned_user = result['ticket'].assigned_user;
                _this.screen_id = result['ticket'].screen_id;
                _this.phone = result['ticket'].assigned_phone;
                _this.distance = result['distance'];
                _this.duration = result['duration'];
                if (result['ticket'].approved == 2) {
                    _this.navCtrl.pop();
                    _this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_2__support_tracker_support_tracker__["a" /* SupportTrackerPage */], { 'ticket_id': _this.navParams.get('ticket_id') });
                }
                _this.map.addMarker({
                    title: 'Support',
                    icon: 'assets/tech-map-icon.png',
                    animation: 'DROP',
                    position: {
                        lat: _this.latitude,
                        lng: _this.longitude,
                    }
                });
                _this.map.addMarker({
                    title: 'Screen',
                    icon: 'assets/client-map-icon.png',
                    // animation: 'DROP',
                    position: {
                        lat: result['ticket'].latitude,
                        lng: result['ticket'].longitude,
                    }
                });
                _this.loading.dismiss();
            });
        });
    };
    VerifySupportPage.prototype.cancelRequest = function () {
        var _this = this;
        var alert = this.alertCtrl.create({
            title: 'Confirm Cancellation',
            message: 'Are you sure you want to cancel this support ticket?',
            buttons: [
                {
                    text: 'No',
                    role: 'cancel',
                    handler: function () {
                        console.log('Cancel clicked');
                    }
                },
                {
                    text: 'Yes',
                    handler: function () {
                        var loading = _this.loadingCtrl.create({
                            content: 'Please wait...'
                        });
                        loading.present();
                        _this.restProvider.declineTicket(_this.navParams.get('ticket_id')).then(function (result) {
                            _this.presentToast(result['msg']);
                            loading.dismiss();
                        });
                    }
                }
            ]
        });
        alert.present();
    };
    VerifySupportPage.prototype.presentToast = function (msg) {
        var _this = this;
        var toast = this.toastCtrl.create({
            message: msg,
            duration: 5000,
            position: 'top'
        });
        toast.onDidDismiss(function () {
            _this.navCtrl.pop();
        });
        toast.present();
    };
    VerifySupportPage.prototype.doRefresh = function (refresher) {
        this.getData();
        refresher.complete();
    };
    VerifySupportPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'page-verify-support',template:/*ion-inline-start:"/Users/admin/Desktop/kitclient/src/pages/verify-support/verify-support.html"*/'<ion-header>\n\n    <ion-navbar>\n      <ion-title>SUPPORT</ion-title>\n    </ion-navbar>\n  \n  </ion-header>\n  <ion-content class="adjustment" align="center" padding>\n      <ion-img class="Logo" align="center" src="assets/kitvision.png"></ion-img>\n      <hr class="logohr">\n      <h2><ion-label align="center" TextDecorations="Underline">GET INSTANT HELP</ion-label>\n      </h2><hr class="logohr">\n\n      <ion-refresher (ionRefresh)="doRefresh($event)">\n          <ion-refresher-content\n                  pullingIcon="arrow-dropdown"\n                  pullingText="Pull to refresh"\n                  refreshingSpinner="circles"\n                  refreshingText="Refreshing...">\n          </ion-refresher-content>\n      </ion-refresher>\n\n      <div class="information">Estimated time {{duration}}</div>\n      <div class="mapImage" id="map" style="height: 60%"></div>\n      <ion-item>\n\n          <img item-start class="personImage" src="{{(avatar==\'\')?\'assets/avatar.png\':avatar}}"/>\n          <h2>{{assigned_user}}</h2>\n          <p >{{phone}}</p>\n      </ion-item>\n  <button class="btnCancelRequest"  (click)="cancelRequest()">CANCEL REQUEST</button>\n</ion-content>\n\n\n<div>\n\n</div>\n<div>\n\n</div>\n'/*ion-inline-end:"/Users/admin/Desktop/kitclient/src/pages/verify-support/verify-support.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["h" /* NavController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["i" /* NavParams */],
            __WEBPACK_IMPORTED_MODULE_3__providers_rest_rest__["a" /* RestProvider */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["k" /* ToastController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["g" /* LoadingController */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["a" /* AlertController */]])
    ], VerifySupportPage);
    return VerifySupportPage;
}());

//# sourceMappingURL=verify-support.js.map

/***/ }),

/***/ 53:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return SupportTrackerPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(9);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__providers_rest_rest__ = __webpack_require__(13);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__feedback_feedback__ = __webpack_require__(54);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};




/**
 * Generated class for the SupportTrackerPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */
var SupportTrackerPage = (function () {
    function SupportTrackerPage(navCtrl, navParams, restProvider, loadingCtrl) {
        var _this = this;
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.restProvider = restProvider;
        this.loadingCtrl = loadingCtrl;
        var loading = this.loadingCtrl.create({
            content: 'Please wait...'
        });
        loading.present();
        this.restProvider.getTimeSinceArrival(this.navParams.get('ticket_id')).then(function (result) {
            _this.time = result['arrived_at'];
            loading.dismiss();
        });
    }
    SupportTrackerPage.prototype.ionViewDidLoad = function () {
        console.log('ionViewDidLoad SupportTrackerPage');
    };
    SupportTrackerPage.prototype.doRefresh = function (refresher) {
        var _this = this;
        this.restProvider.getTimeSinceArrival(this.navParams.get('ticket_id')).then(function (result) {
            if (result['completed_work']) {
                _this.navCtrl.pop();
                _this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_3__feedback_feedback__["a" /* FeedbackPage */], { 'ticket_id': _this.navParams.get('ticket_id') });
                return;
            }
            _this.time = result['arrived_at'];
            refresher.complete();
        });
    };
    SupportTrackerPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'page-support-tracker',template:/*ion-inline-start:"/Users/admin/Desktop/kitclient/src/pages/support-tracker/support-tracker.html"*/'<!--\n  Generated template for the SupportTrackerPage page.\n\n  See http://ionicframework.com/docs/components/#navigation for more info on\n  Ionic pages and navigation.\n-->\n<ion-header>\n\n  <ion-navbar>\n    <ion-title>SUPPORT</ion-title>\n  </ion-navbar>\n\n</ion-header>\n<ion-content align="center" padding>\n    <ion-img class="Logo" align="center" src="assets/kitvision.png"></ion-img>\n    <hr class="logohr">\n    <h2><ion-label align="center" TextDecorations="Underline">WORK IN PROGRESS</ion-label>\n    </h2><hr class="logohr">\n    <ion-refresher (ionRefresh)="doRefresh($event)">\n        <ion-refresher-content\n                pullingIcon="arrow-dropdown"\n                pullingText="Pull to refresh"\n                refreshingSpinner="circles"\n                refreshingText="Refreshing...">\n        </ion-refresher-content>\n    </ion-refresher>\n\n    <ion-img class="tracker" src="assets/progress.png"></ion-img>\n    <h3>TIME SINCE ARRIVAL</h3>\n    <h3><b>{{time}}</b> Min</h3>\n\n</ion-content>\n'/*ion-inline-end:"/Users/admin/Desktop/kitclient/src/pages/support-tracker/support-tracker.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["h" /* NavController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["i" /* NavParams */],
            __WEBPACK_IMPORTED_MODULE_2__providers_rest_rest__["a" /* RestProvider */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["g" /* LoadingController */]])
    ], SupportTrackerPage);
    return SupportTrackerPage;
}());

//# sourceMappingURL=support-tracker.js.map

/***/ }),

/***/ 54:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return FeedbackPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(9);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__providers_rest_rest__ = __webpack_require__(13);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};



/**
 * Generated class for the FeedbackPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */
var FeedbackPage = (function () {
    function FeedbackPage(navCtrl, navParams, alertCtrl, toastCtrl, restProvider) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.alertCtrl = alertCtrl;
        this.toastCtrl = toastCtrl;
        this.restProvider = restProvider;
        this.feedback = '';
        this.rating = 3;
    }
    FeedbackPage.prototype.ionViewDidLoad = function () {
        console.log('ionViewDidLoad FeedbackPage');
    };
    FeedbackPage.prototype.saveFeedBack = function () {
        var _this = this;
        if (this.feedback.length == 0) {
            var alert_1 = this.alertCtrl.create({
                title: 'Feedback',
                subTitle: 'Please Add feedback description',
                buttons: ['Dismiss']
            });
            alert_1.present();
            return;
        }
        this.restProvider.saveFeedBack(this.navParams.get('ticket_id'), this.feedback, this.rating).then(function (result) {
            var toast = _this.toastCtrl.create({
                message: result['msg'],
                duration: 5000,
                position: 'top'
            });
            toast.onDidDismiss(function () {
                _this.navCtrl.pop();
            });
            toast.present();
        });
    };
    FeedbackPage.prototype.updRating = function (rating) {
        this.rating = rating;
    };
    FeedbackPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'page-feedback',template:/*ion-inline-start:"/Users/admin/Desktop/kitclient/src/pages/feedback/feedback.html"*/'<!--\n  Generated template for the FeedbackPage page.\n\n  See http://ionicframework.com/docs/components/#navigation for more info on\n  Ionic pages and navigation.\n-->\n<ion-header>\n\n  <ion-navbar>\n    <ion-title>SUPPORT</ion-title>\n  </ion-navbar>\n\n</ion-header>\n<ion-content class="adjustment" align="center" padding>\n  <ion-img class="Logo" align="center" src="assets/kitvision.png"></ion-img>\n  <hr class="logohr">\n  <h2><ion-label align="center" TextDecorations="Underline">JOB DONE</ion-label>\n  </h2>\n  <hr class="logohr">\n  <ion-img class="job-tick" src="assets/tick.png"></ion-img>\n\n<ion-label class="lbl-rating">RATE SERVICE</ion-label>\n  <div class="rating-feedback">\n    <ion-icon name="star" *ngIf="rating > 0" (click)="updRating(0)"></ion-icon>\n    <ion-icon name="star" *ngIf="rating > 1" (click)="updRating(1)"></ion-icon>\n    <ion-icon name="star" *ngIf="rating > 2" (click)="updRating(2)"></ion-icon>\n    <ion-icon name="star" *ngIf="rating > 3" (click)="updRating(3)"></ion-icon>\n    <ion-icon name="star" *ngIf="rating > 4" (click)="updRating(4)"></ion-icon>\n    <ion-icon name="star-outline" *ngIf="rating < 1" (click)="updRating(1)"></ion-icon>\n    <ion-icon name="star-outline" *ngIf="rating < 2" (click)="updRating(2)"></ion-icon>\n    <ion-icon name="star-outline" *ngIf="rating < 3" (click)="updRating(3)"></ion-icon>\n    <ion-icon name="star-outline" *ngIf="rating < 4" (click)="updRating(4)"></ion-icon>\n    <ion-icon name="star-outline" *ngIf="rating < 5" (click)="updRating(5)"></ion-icon>\n  </div>\n\n  <ion-label class="lbl-comments">ADDITIONAL COMMENTS</ion-label>\n  <ion-item class="feedback-wrapper">\n\n    <ion-textarea class="feedback-desc" placeholder="Describe Your Feedback"  [(ngModel)]="feedback" maxlength="250"></ion-textarea>\n  </ion-item>\n\n  <button round class="tecBtn" (click)="saveFeedBack()" >Submit</button>\n\n</ion-content>\n'/*ion-inline-end:"/Users/admin/Desktop/kitclient/src/pages/feedback/feedback.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["h" /* NavController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["i" /* NavParams */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["a" /* AlertController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["k" /* ToastController */],
            __WEBPACK_IMPORTED_MODULE_2__providers_rest_rest__["a" /* RestProvider */]])
    ], FeedbackPage);
    return FeedbackPage;
}());

//# sourceMappingURL=feedback.js.map

/***/ }),

/***/ 83:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return HomePage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(9);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__dashboard_dashboard__ = __webpack_require__(106);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__providers_rest_rest__ = __webpack_require__(13);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__resetpassword_resetpassword__ = __webpack_require__(111);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};





var HomePage = (function () {
    function HomePage(navCtrl, restProvider, toastCtrl) {
        /* this.currentUserData = JSON.parse(localStorage.getItem('currentUser'));
        if(this.currentUserData.token==null)
        {
          
        }
        else
        {
          this.navCtrl.push(DashboardPage);
        }*/
        this.navCtrl = navCtrl;
        this.restProvider = restProvider;
        this.toastCtrl = toastCtrl;
        this.credential = { username: '', password: '' };
    }
    HomePage.prototype.verifyLogin = function () {
        var _this = this;
        if (this.credential.username == '' || this.credential.password == '') {
            this.presentToast('Please enter username and password');
            return;
        }
        this.restProvider.login(this.credential).then(function (result) {
            if (result['error']) {
                _this.presentToast("Please give valid username and password");
            }
            else {
                _this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_2__dashboard_dashboard__["a" /* DashboardPage */]);
            }
        }, function (err) {
            console.log(err);
        });
        //this.navCtrl.push(DashboardPage);
    };
    HomePage.prototype.presentToast = function (msg) {
        var toast = this.toastCtrl.create({
            message: msg,
            duration: 2000,
            position: 'top'
        });
        toast.present();
    };
    HomePage.prototype.reset = function () {
        this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_4__resetpassword_resetpassword__["a" /* ResetpasswordPage */]);
    };
    HomePage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'page-home',template:/*ion-inline-start:"/Users/admin/Desktop/kitclient/src/pages/home/home.html"*/'<ion-content align="center" hide-tabs>\n\n<br><br><br><br><br><br><br><br><br>\n    <ion-img  align="center" class="Logo" src="assets/kitvision.png"></ion-img>\n    <br><br><br><br>\n  <p>{{ alerts }} </p>\n      <ion-input class="input" placeholder="UserName" [(ngModel)]="credential.username"></ion-input>\n   \n      <ion-input type="password" placeholder="Password" [(ngModel)]="credential.password"></ion-input>\n    \n  <br/>\n  <button  round class="buttonLogin" align="center" (click) = "verifyLogin()">LOGIN</button>\n  <br/>\n    <a (click)="reset()">\n        <u><ion-label class="forgetPassword">Forget Your Password!</ion-label></u>\n    </a>\n</ion-content>\n  '/*ion-inline-end:"/Users/admin/Desktop/kitclient/src/pages/home/home.html"*/
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["h" /* NavController */], __WEBPACK_IMPORTED_MODULE_3__providers_rest_rest__["a" /* RestProvider */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["k" /* ToastController */]])
    ], HomePage);
    return HomePage;
}());

//# sourceMappingURL=home.js.map

/***/ })

},[218]);
//# sourceMappingURL=main.js.map