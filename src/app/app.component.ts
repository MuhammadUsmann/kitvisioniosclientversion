import { Component } from '@angular/core';
import { Platform } from 'ionic-angular';
import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';
import { Network } from '@ionic-native/network';
import { AlertController } from "ionic-angular";

import { HomePage } from '../pages/home/home';
@Component({
  templateUrl: 'app.html'
})
export class MyApp {
  rootPage:any = HomePage;

  constructor(platform: Platform, statusBar: StatusBar, splashScreen: SplashScreen , network: Network, alertCtrl: AlertController) {
    platform.ready().then(() => {
      this.rootPage = HomePage;
      statusBar.styleDefault();
      splashScreen.hide();

        let disconnectSubscription = network.onDisconnect().subscribe(() => {
            let alert = alertCtrl.create({
                title: 'Offline',
                subTitle: 'No Internet Connection is available the app will not work',
                buttons: ['Ok']
            });
            alert.present();
        });

    });
  }
}

