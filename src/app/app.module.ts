import { BrowserModule } from '@angular/platform-browser';
import { ErrorHandler, NgModule } from '@angular/core';
import { IonicApp, IonicErrorHandler, IonicModule } from 'ionic-angular';
import { SplashScreen } from '@ionic-native/splash-screen';
import { StatusBar } from '@ionic-native/status-bar';
import { Geolocation} from "@ionic-native/geolocation";
import { Push, PushObject, PushOptions } from '@ionic-native/push';
import { GoogleMaps } from "@ionic-native/google-maps";
import { Network } from '@ionic-native/network';

import { MyApp } from './app.component';
import { HomePage } from '../pages/home/home';

import { DashboardPage } from '../pages/dashboard/dashboard';

import { TrbShootPage } from '../pages/trb-shoot/trb-shoot';
import { ChatPage } from '../pages/chat/chat';
import { SupportPage } from '../pages/support/support';
import { HistoryPage } from '../pages/history/history';
import { ProfilePage } from '../pages/profile/profile';
import { LoginPage } from '../pages/login/login';
import { VerifySupportPage } from '../pages/verify-support/verify-support';
import { SupportTrackerPage } from '../pages/support-tracker/support-tracker';
import { OfflinePage } from '../pages/offline/offline';
import { DownloadsPage } from '../pages/downloads/downloads';
import { RestProvider } from '../providers/rest/rest';
import { HttpClientModule } from '@angular/common/http';
import { ResetpasswordPage } from '../pages/resetpassword/resetpassword';
import { FeedbackPage} from "../pages/feedback/feedback";

@NgModule({
  declarations: [
    MyApp,
   HomePage,
   DashboardPage,
   TrbShootPage,
   ChatPage,
   SupportPage,
   HistoryPage,
   ProfilePage,
   LoginPage,
   VerifySupportPage,
   SupportTrackerPage,
   OfflinePage,
   ResetpasswordPage,
   DownloadsPage,
   FeedbackPage,
  ],
  imports: [
    BrowserModule,
    HttpClientModule,
    IonicModule.forRoot(MyApp)
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    MyApp,
    HomePage,
    DashboardPage,
    TrbShootPage,
    ChatPage,
    SupportPage,
    HistoryPage,
    ProfilePage,
    LoginPage,
    VerifySupportPage,
    SupportTrackerPage,
    OfflinePage,
    DownloadsPage,
    ResetpasswordPage,
    FeedbackPage,
  ],
  providers: [
    StatusBar,
    SplashScreen,
      Geolocation,
      GoogleMaps,
      Push,
    {provide: ErrorHandler, useClass: IonicErrorHandler},
    RestProvider,
      Network,
  ]
})
export class AppModule {}
