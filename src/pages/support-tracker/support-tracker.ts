import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, LoadingController } from 'ionic-angular';
import { RestProvider} from "../../providers/rest/rest";
import { FeedbackPage } from "../feedback/feedback";

/**
 * Generated class for the SupportTrackerPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-support-tracker',
  templateUrl: 'support-tracker.html',
})
export class SupportTrackerPage {
  time:any;
  constructor(public navCtrl: NavController, public navParams: NavParams,
              private restProvider: RestProvider, private loadingCtrl: LoadingController) {
      let loading = this.loadingCtrl.create({
          content: 'Please wait...'
      });

      loading.present();
      this.restProvider.getTimeSinceArrival(this.navParams.get('ticket_id')).then((result)=>{
       this.time = result['arrived_at'];
       loading.dismiss();
     });
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad SupportTrackerPage');
  }
    doRefresh(refresher) {
        this.restProvider.getTimeSinceArrival(this.navParams.get('ticket_id')).then((result)=>{
            if(result['completed_work']){
                this.navCtrl.pop();
                this.navCtrl.push(FeedbackPage,{'ticket_id':this.navParams.get('ticket_id')});
                return;
            }

            this.time = result['arrived_at'];
            refresher.complete();
        });
    }

}
