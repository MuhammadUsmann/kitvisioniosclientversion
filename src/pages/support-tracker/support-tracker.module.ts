import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { SupportTrackerPage } from './support-tracker';

@NgModule({
  declarations: [
    SupportTrackerPage,
  ],
  imports: [
    IonicPageModule.forChild(SupportTrackerPage),
  ],
})
export class SupportTrackerPageModule {}
