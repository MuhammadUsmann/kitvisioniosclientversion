import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, App,ToastController } from 'ionic-angular';
import { AlertController } from 'ionic-angular';
import { HomePage } from '../home/home';
import { RestProvider } from '../../providers/rest/rest';
import {DashboardPage} from "../dashboard/dashboard";

/**
 * Generated class for the ProfilePage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-profile',
  templateUrl: 'profile.html',
})
export class ProfilePage {
  password_data ={con_password:'',password:''};
  phone_data ={phone:''};
  currentUserData: string;

  constructor(public alertCtrl: AlertController,public navCtrl: NavController, public navParams: NavParams,public restProvider: RestProvider,public app: App, public toastCtrl: ToastController) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad ProfilePage');
  }

  logout(){
      let confirm = this.alertCtrl.create({
        title: 'Are you sure?',
        message: 'Do you want to Logout.',
        buttons: [
          {
            text: 'Cancel',
            handler: () => {
              console.log('Disagree clicked');
            }
          },
          {
            text: 'Logout',
            handler: () => {
                this.restProvider.setChatStatus(0)
                    .then((result) => {
                        this.restProvider.logout();
                        this.app.getRootNav().setRoot(HomePage);
                    }, (err) => {
                        console.log(err);
                    });
            }
          }
        ]
      });
      confirm.present();
  }

  changepwd(){
     if(this.password_data.password != this.password_data.con_password){
         let confirm = this.alertCtrl.create({
             title: 'Password Mismatch',
             message: 'Password didnot match with confirm password',
             buttons: [
                 {
                     text: 'OK',
                     handler: () => {
                        //do nothing
                     }
                 }
             ]
         });
         confirm.present();
         return;
     }

      if(this.password_data.password == '' || this.password_data.con_password == ''){
          let confirm = this.alertCtrl.create({
              title: 'Enter Password',
              message: 'Please enter password',
              buttons: [
                  {
                      text: 'OK',
                      handler: () => {
                          //do nothing
                      }
                  }
              ]
          });
          confirm.present();
          return;
      }

      this.restProvider.changePassword(this.password_data).then((result) => {
          if (result['error']) {
              this.presentToast(result['error']);
          }
          else {
              let confirm = this.alertCtrl.create({
                  title: 'Password Changed',
                  message: result['msg'],
                  buttons: [
                      {
                          text: 'OK',
                          handler: () => {
                              this.restProvider.logout();
                              this.app.getRootNav().setRoot(HomePage);
                          }
                      }
                  ]
              });
              confirm.present();


          }
      },(err) => {
          console.log(err);
      });
  }

    presentToast(msg) {
        let toast = this.toastCtrl.create({
            message: msg,
            duration: 5000,
            position:'top'
        });
        toast.present();
    }

  changePhone(){

      if(this.phone_data.phone== ''){
          let confirm = this.alertCtrl.create({
              title: 'Enter Phone',
              message: 'Please enter phone number',
              buttons: [
                  {
                      text: 'OK',
                      handler: () => {
                          //do nothing
                      }
                  }
              ]
          });
          confirm.present();
          return;
      }

      this.restProvider.changePhone(this.phone_data.phone).then((result) => {
          if (result['error']) {
              this.presentToast(result['error']);
          }
          else {
              this.presentToast(result['msg']);
              this.phone_data.phone = '';
          }
      },(err) => {
          console.log(err);
      });
  }
}
