import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams,ToastController, App, AlertController, LoadingController } from 'ionic-angular';
import { VerifySupportPage } from '../verify-support/verify-support';
import { HistoryPage } from '../history/history';
import { RestProvider } from '../../providers/rest/rest';
/**
 * Generated class for the SupportPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-support',
  templateUrl: 'support.html',
})
export class SupportPage {

  screens:any;
  currentUserData :any;
  ticket = {
    "api_token": "",
    "screen_id": "",
    "prob_desc": ""
  };
  constructor(public navCtrl: NavController, public navParams: NavParams,
              public restProvider: RestProvider, private toastCtrl:ToastController,
              private alertCtrl: AlertController,
              public app: App, private loadingCtrl: LoadingController) {
    this.currentUserData = JSON.parse(localStorage.getItem('currentUser'));
    this.ticket.api_token = this.currentUserData.token;
    this.getAllScreens(this.currentUserData.token);
    
  }


verifySupport()
{
  this.navCtrl.push(VerifySupportPage);
}


  ionViewDidLoad() {
    console.log('ionViewDidLoad SupportPage');
  }

  getAllScreens(token)
  {
      this.restProvider.getScreens(token)
    .then(data => {
      this.screens = data;
    });
  }

  addUserTicket()
  {
      if(this.ticket.screen_id == ""){
          let alert = this.alertCtrl.create({
              title: 'Choose Screen',
              subTitle: 'Please choose screen',
              buttons: ['Dismiss']
          });
          alert.present();

          return;

      }

      if(this.ticket.prob_desc == ""){
          let alert = this.alertCtrl.create({
              title: 'Add Description',
              subTitle: 'Please Add problem Description',
              buttons: ['Dismiss']
          });
          alert.present();

          return;

      }

      let loading = this.loadingCtrl.create({
          content: 'Please wait...'
      });

      loading.present();

    this.restProvider.addTicket(this.ticket)
    .then((result) => {
     
    if(result['error'])
    {
      this.presentToast("Something goes wrong");
     
    }
    else
    {
       this.presentToast(result['msg']);
       this.ticket.prob_desc = '';
    }
    loading.dismiss();
     
  }, (err) => {
    console.log(err);
  });
  }

 presentToast(msg) {
    let toast = this.toastCtrl.create({
      message: msg,
      duration: 5000,
      position: 'top'
    });

     toast.onDidDismiss(() => {
         this.app.getRootNav().getActiveChildNav().select(3);
     });

    toast.present();
  }
}
