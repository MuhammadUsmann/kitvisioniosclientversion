import { Component } from '@angular/core';
import { NavController,ToastController } from 'ionic-angular';
import { DashboardPage } from '../dashboard/dashboard';
import { RestProvider } from '../../providers/rest/rest';
import { ResetpasswordPage } from '../resetpassword/resetpassword';

@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})
export class HomePage {
credential ={username:'',password:''};

  constructor(public navCtrl: NavController, public restProvider: RestProvider, private toastCtrl:ToastController) {
    /* this.currentUserData = JSON.parse(localStorage.getItem('currentUser'));
    if(this.currentUserData.token==null)
    {
      
    }
    else
    {
      this.navCtrl.push(DashboardPage);
    }*/

  }

  verifyLogin(){

      if(this.credential.username == '' || this.credential.password == ''){
          this.presentToast('Please enter username and password');
          return;
      }

    this.restProvider.login(this.credential).then((result) => {
    if(result['error'])
    {
      this.presentToast("Please give valid username and password");
     
    }
    else
    {
        this.navCtrl.push(DashboardPage);
    }
  }, (err) => {
    console.log(err);
  });
    //this.navCtrl.push(DashboardPage);
  }

   presentToast(msg) {
    let toast = this.toastCtrl.create({
      message: msg,
      duration: 2000,
      position: 'top'
    });
    toast.present();
  }

    reset(){
        this.navCtrl.push(ResetpasswordPage);
    }
}
