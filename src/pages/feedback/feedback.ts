import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams,AlertController,ToastController } from 'ionic-angular';
import { RestProvider } from "../../providers/rest/rest";

/**
 * Generated class for the FeedbackPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-feedback',
  templateUrl: 'feedback.html',
})
export class FeedbackPage {
  feedback:string='';
  rating:number = 3;

  constructor(public navCtrl: NavController, public navParams: NavParams,
              private alertCtrl: AlertController, private toastCtrl: ToastController,
              private restProvider: RestProvider) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad FeedbackPage');
  }

  saveFeedBack(){
      if(this.feedback.length == 0){
          let alert = this.alertCtrl.create({
              title: 'Feedback',
              subTitle: 'Please Add feedback description',
              buttons: ['Dismiss']
          });
          alert.present();

          return;
      }

      this.restProvider.saveFeedBack(this.navParams.get('ticket_id'),this.feedback, this.rating).then((result)=>{
          let toast = this.toastCtrl.create({
              message: result['msg'],
              duration: 5000,
              position: 'top'
          });

          toast.onDidDismiss(() => {
              this.navCtrl.pop();
          });

          toast.present();

      });


  }

  updRating(rating){
    this.rating = rating;
  }

}
