import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { TrbShootPage } from './trb-shoot';

@NgModule({
  declarations: [
    TrbShootPage,
  ],
  imports: [
    IonicPageModule.forChild(TrbShootPage),
  ],
})
export class TrbShootPageModule {}
