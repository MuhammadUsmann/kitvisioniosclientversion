import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams,ToastController } from 'ionic-angular';
import { RestProvider } from '../../providers/rest/rest';
/**
 * Generated class for the TrbShootPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-trb-shoot',
  templateUrl: 'trb-shoot.html',
})
export class TrbShootPage {
files:any;
currentUserData:any;
searchTerm: string = '';
allFiles:any;
  constructor(public navCtrl: NavController, public navParams: NavParams, public restProvider: RestProvider, private toastCtrl:ToastController) {
    this.currentUserData = JSON.parse(localStorage.getItem('currentUser'));
    this.getAllFiles(this.currentUserData.token);
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad TrbShootPage');
  }
    getAllFiles(token)
  {
      this.restProvider.getFiles(token)
    .then((result) => {
    if(result['error'])
    {
      this.presentToast("Something goes wrong");
     
    }
    else
    {
       this.files = result;
       this.allFiles = result;
    }
     
  }, (err) => {
    console.log(err);
  });
  }

 presentToast(msg) {
    let toast = this.toastCtrl.create({
      message: msg,
      duration: 5000
    });
    toast.present();
  }

  filterItems(searchTerm){
 //this.getAllFiles(this.currentUserData.token);
        return this.files.filter((item) => {
            return item.file_name.toLowerCase().indexOf(searchTerm.toLowerCase()) > -1;
        });    
 
    }

    setFilteredItems() {
        this.files = this.allFiles;
      //this.getAllFiles(this.currentUserData.token);
        this.files = this.filterItems(this.searchTerm);
 
    }
}
