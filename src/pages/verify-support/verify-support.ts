import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams,ToastController, AlertController, LoadingController } from 'ionic-angular';
import { SupportTrackerPage } from '../support-tracker/support-tracker';
import {RestProvider} from "../../providers/rest/rest";
import {
    GoogleMaps,
    GoogleMap,
    GoogleMapsEvent,
    GoogleMapOptions,
    CameraPosition,
    MarkerOptions,
    Marker, GoogleMapsAnimation
} from '@ionic-native/google-maps';

/**
 * Generated class for the VerifySupportPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-verify-support',
  templateUrl: 'verify-support.html',
})
export class VerifySupportPage {
    map: GoogleMap;

    ticket_id:string;
    address:string;
    avatar:string;
    assigned_user:string;
    screen_id:string;
    distance:string;
    duration:string;
    latitude:any;
    longitude:any;
    phone:string;

  constructor(public navCtrl: NavController, public navParams: NavParams,
              private restProvider: RestProvider, private toastCtrl:ToastController, private loadingCtrl: LoadingController,
              private alertCtrl: AlertController) {

        //this.getData();
          this.loading = this.loadingCtrl.create({
          content: 'Please wait...'
      });


      this.loading.present();
      console.log("Alert");
      this.restProvider.getTracking(this.navParams.get('tech_id')).then((result)=>{
          this.latitude = result['latitude'];
          this.longitude = result['longitude'];

          this.loadMap();
      });
  }
loading:any;
  getData(){
      this.loading = this.loadingCtrl.create({
          content: 'Please wait...'
      });

      this.loading.present();
      this.restProvider.getTracking(this.navParams.get('tech_id')).then((result)=>{
          this.latitude = result['latitude'];
          this.longitude = result['longitude'];

          this.loadMap();
      });
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad VerifySupportPage');
  }
  supportTracker()
  {
    this.navCtrl.push(SupportTrackerPage);
  }

    loadMap(){
        let mapOptions: GoogleMapOptions = {
            camera: {
                target: {
                    lat: this.latitude,
                    lng: this.longitude,
                },
                zoom: 12,
                tilt: 30
            }
        };

        let element: HTMLElement = document.getElementById('map');
        this.map = GoogleMaps.create(element,mapOptions);
        // this.map = GoogleMaps.create('map', mapOptions);

        // Wait the MAP_READY before using any methods.
        this.map.one(GoogleMapsEvent.MAP_READY)
            .then(() => {

this.restProvider.getTicketDetails(this.navParams.get('ticket_id'),this.latitude, this.longitude).then((result)=>{
              this.ticket_id = result['ticket'].ticket_id;
              this.address = result['ticket'].address;
              this.avatar = result['ticket'].avatar;
              this.assigned_user = result['ticket'].assigned_user;
              this.screen_id = result['ticket'].screen_id;
              this.phone = result['ticket'].assigned_phone;
              this.distance = result['distance'];
              this.duration = result['duration'];

              if(result['ticket'].approved == 2){
                  this.navCtrl.pop();
                  this.navCtrl.push(SupportTrackerPage,{'ticket_id':this.navParams.get('ticket_id')});
              }


              this.map.addMarker({
                  title: 'Support',
                  icon: 'assets/tech-map-icon.png',
                  animation: 'DROP',
                  position: {
                      lat: this.latitude,
                      lng: this.longitude,
                  }
              });

              this.map.addMarker({
                  title: 'Screen',
                  icon: 'assets/client-map-icon.png',
                  // animation: 'DROP',
                  position: {
                      lat: result['ticket'].latitude,
                      lng: result['ticket'].longitude,
                  }
              });
              this.loading.dismiss();
          });

            });
    }

    cancelRequest(){

        let alert = this.alertCtrl.create({
            title: 'Confirm Cancellation',
            message: 'Are you sure you want to cancel this support ticket?',
            buttons: [
                {
                    text: 'No',
                    role: 'cancel',
                    handler: () => {
                        console.log('Cancel clicked');
                    }
                },
                {
                    text: 'Yes',
                    handler: () => {
                        let loading = this.loadingCtrl.create({
                            content: 'Please wait...'
                        });

                        loading.present();
                        this.restProvider.declineTicket(this.navParams.get('ticket_id')).then((result)=>{
                            this.presentToast(result['msg']);
                            loading.dismiss();
                        });
                    }
                }
            ]
        });
        alert.present();

    }

    presentToast(msg) {
        let toast = this.toastCtrl.create({
            message: msg,
            duration: 5000,
            position:'top'
        });
        toast.onDidDismiss(() => {
            this.navCtrl.pop();
        });
        toast.present();
    }

    doRefresh(refresher) {
      this.getData();
      refresher.complete()
    }

}
