import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { VerifySupportPage } from './verify-support';

@NgModule({
  declarations: [
    VerifySupportPage,
  ],
  imports: [
    IonicPageModule.forChild(VerifySupportPage),
  ],
})
export class VerifySupportPageModule {}
