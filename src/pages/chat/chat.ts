import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams,ToastController, AlertController } from 'ionic-angular';
import { OfflinePage } from '../offline/offline';
import { RestProvider } from '../../providers/rest/rest';

/**
 * Generated class for the ChatPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-chat',
  templateUrl: 'chat.html',
})
export class ChatPage {
  offline:number = 3;
  messages:any;
  message_data:string;

  constructor(public navCtrl: NavController,
              public navParams: NavParams,
              public restProvider: RestProvider,
              public alertCtrl: AlertController,
              public toastCtrl: ToastController) {

  }

  loadchat(){
      this.restProvider.getMessages()
          .then((result) => {
              if(result['error'])
              {
                  this.presentToast("Something goes wrong");

              }
              else
              {
                 /* Array.prototype.sort.call(result, function (a, b) {
                      if (a.chat_id > b.chat_id) {
                          return 1;
                      }
                      if (a.chat_id < b.chat_id) {
                          return -1;
                      }
                      return 0;
                  });*/
                  this.messages = result;
              }

          }, (err) => {
              console.log(err);
          });
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad ChatPage');
  }

  ionViewWillEnter(){

      this.restProvider.getChatStatus()
          .then((result) => {
              if(result['available'] > 0) {
                  if(this.restProvider.thread_id == 'none') {
                      this.offline = 2;
                  }
                  else
                  {
                      this.offline = 0;
                      this.loadchat();
                  }
              }
              else {
                  this.offline = 1;
              }
          }, (err) => {
              console.log(err);
          });
  }

  sendFirstMsg(){
      this.restProvider.sendMessage(this.message_data)
          .then((result) => {
              if(result['error'])
              {
                  this.presentToast("Something goes wrong");

              }
              else
              {
                  this.presentToast(result['msg']);
                  this.message_data= "";
                  this.restProvider.thread_id = result['thread_id'];
                  this.offline = 0;
                  this.loadchat();
              }

          }, (err) => {
              console.log(err);
          });
  }

    sendmsg(){
        this.restProvider.sendMessage(this.message_data)
            .then((result) => {
                if(result['error'])
                {
                    this.presentToast("Something goes wrong");

                }
                else
                {
                    this.presentToast(result['msg']);
                    this.message_data= "";
                    this.loadchat();
                }

            }, (err) => {
                console.log(err);
            });
    }

    presentToast(msg) {
        let toast = this.toastCtrl.create({
            message: msg,
            duration: 5000,
            position:'top'
        });
        toast.present();
    }

    doRefresh(refresher) {

        this.restProvider.getChatStatus()
            .then((result) => {
                if(result['available'] > 0) {
                    if(this.restProvider.thread_id == 'none') {
                        this.offline = 2;
                    }
                    else
                    {
                        this.offline = 0;
                        this.loadchat();
                    }
                }
                else {
                    this.offline = 1;
                }
                refresher.complete();
            }, (err) => {
                console.log(err);
                refresher.complete();
            });
    }

    closeChat(){

        let alert = this.alertCtrl.create({
            title: 'Confirm Closing',
            message: 'Do you want to close this chat?',
            buttons: [
                {
                    text: 'No',
                    role: 'cancel',
                    handler: () => {
                        console.log('no clicked');
                    }
                },
                {
                    text: 'Yes',
                    handler: () => {
                        this.restProvider.closeThread().then((result)=>{
                            this.presentToast(result['msg']);
                            this.message_data= "";
                            this.restProvider.thread_id = 'none';
                            this.offline = 2;
                        });
                    }
                }
            ]
        });
        alert.present();


    }



}
