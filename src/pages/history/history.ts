import { Component } from '@angular/core';
import {Img, IonicPage, NavController, NavParams,ToastController, AlertController } from 'ionic-angular';
import { RestProvider } from '../../providers/rest/rest';
import { VerifySupportPage } from "../verify-support/verify-support";
import { SupportTrackerPage } from "../support-tracker/support-tracker";
import { FeedbackPage} from "../feedback/feedback";

/**
 * Generated class for the HistoryPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-history',
  templateUrl: 'history.html',
})
export class HistoryPage {

history:any;
currentUserData:any;
  constructor(public navCtrl: NavController, public navParams: NavParams,public restProvider: RestProvider,
              private toastCtrl:ToastController, private alertCtrl: AlertController) {
/*this.currentUserData = JSON.parse(localStorage.getItem('currentUser'));
    this.getAllHistory(this.currentUserData.token);*/
 }



  ionViewDidLoad() {
    console.log('ionViewDidLoad HistoryPage');
  }

    ionViewWillEnter() {
        this.currentUserData = JSON.parse(localStorage.getItem('currentUser'));
        this.getAllHistory(this.currentUserData.token);
    }
   getAllHistory(token)
  {
      this.restProvider.getHistory(token)
    .then((result) => {
     
    if(result['error'])
    {
      this.presentToast("Something goes wrong");
     
    }
    else
    {
       this.history = result;
    }
     
  }, (err) => {
    console.log(err);
  });
  }

 presentToast(msg) {
    let toast = this.toastCtrl.create({
      message: msg,
      duration: 2000,
        position: 'top'
    });
    toast.present();
  }
    ticketclick(ticket_id,approved,tech_id,feedback){

      if(approved == 1){
          this.navCtrl.push(VerifySupportPage,{'ticket_id':ticket_id,'tech_id':tech_id});
      }
      else if(approved == 2){
          this.navCtrl.push(SupportTrackerPage,{'ticket_id':ticket_id,'tech_id':tech_id});
      }
      else if(approved == 3){
          if(feedback == 0)
            this.navCtrl.push(FeedbackPage,{'ticket_id':ticket_id,'tech_id':tech_id});
          else{
              let alert = this.alertCtrl.create({
                  title: 'Feedback Given',
                  subTitle: 'Feedback already given against this ticket',
                  buttons: ['Dismiss']
              });
              alert.present();

          }
      }

    }

    doRefresh(refresher) {
        this.getAllHistory(this.currentUserData.token);
        refresher.complete();

    }

}
