import { Component } from '@angular/core';
import {IonicPage, NavController, NavParams, AlertController, Alert} from 'ionic-angular';

import { TrbShootPage } from '../trb-shoot/trb-shoot';
import { ChatPage } from '../chat/chat';
import { SupportPage } from '../support/support';
import { HistoryPage } from '../history/history';
import { RestProvider } from "../../providers/rest/rest";
import { ProfilePage } from '../profile/profile';
import { Push, PushObject, PushOptions } from '@ionic-native/push';
import { LoginPage } from '../login/login';
import { DownloadsPage } from '../downloads/downloads';
import {Observable} from "rxjs/Observable";


/**
 * Generated class for the DashboardPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-dashboard',
  templateUrl: 'dashboard.html',
})
export class DashboardPage {
  public task:any;
  tab1Root = TrbShootPage;
  tab2Root = ChatPage;
  tab3Root = SupportPage;
  tab4Root = HistoryPage;
  tab5Root = ProfilePage;
 // tab6Root = LoginPage;


  constructor(public navCtrl: NavController, public navParams: NavParams, public restProvider:RestProvider,
              private push: Push, private alertCtrl: AlertController) {
      this.restProvider.setChatStatus(1)
          .then((result) => {
              //do nothing

          }, (err) => {
              console.log(err);
          });

 this.initPush();
      // to check if we have permission
  /*   
      this.push.hasPermission()
          .then((res: any) => {

              if (res.isEnabled) {
                  this.initPush();
              } else {
                  console.log('We do not have permission to send push notifications');
              }

          });
*/
      //for chat status
      this.task = setInterval(() => {
          this.restProvider.setChatStatus(1);
      }, 30000);
  }

    initPush(){
        const options: PushOptions = {
            android: {
                senderID: '800934935444',
            },
            ios: {
                alert: 'true',
                badge: true,
                sound: 'false'
            },
            windows: {},
            browser: {
                pushServiceURL: 'http://push.api.phonegap.com/v1/push',
                applicationServerKey:'AIzaSyB1IB67RCeadDQ-R27weVrUdzn9tv3VFJg',
            }
        };

        const pushObject: PushObject = this.push.init(options);

        pushObject.on('notification').subscribe((notification: any) => {

            /*if(notification.message.indexOf('Chat') != -1)
                this.presentChatAlert(notification.message);
            else if(notification.message.indexOf('Ticket') != -1)
                this.presentTicketAlert(notification.message);
            else*/
                this.presentAlert(notification.message, notification.title);

        });

        pushObject.on('registration').subscribe((registration: any) => {
            this.restProvider.setChatToken(registration.registrationId).then((result)=>{
                console.log(result['msg']);
            });
            console.log('Device registered', registration.registrationId);
        });

        pushObject.on('error').subscribe(error => console.error('Error with Push plugin', error));
    }

    presentAlert(msg,title) {
        let alert = this.alertCtrl.create({
            title: title,
            subTitle: msg,
            buttons: ['Dismiss']
        });
        alert.present();
    }

  ionViewDidLoad() {
    console.log('ionViewDidLoad DashboardPage');
  }

}
