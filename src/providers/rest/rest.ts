import { HttpClient, HttpParams } from '@angular/common/http';
import { Injectable } from '@angular/core';

/*
  Generated class for the RestProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/
@Injectable()
export class RestProvider {
apiUrl = 'http://app.kitvision.org/api';
public token: string;
public username: string;
public thread_id:string = 'none';

  constructor(public http: HttpClient) {
    console.log('Hello RestProvider Provider');
  }

  login(data){
    this.username = data.username;
    return new Promise((resolve, reject) => {
    this.http.post(this.apiUrl+'/login', JSON.stringify(data))
      .subscribe(res => {
        // login successful if there's a jwt token in the response
         const token = res['token'];
          if (token) {
              // set token property
              this.token = token;

              // store username and jwt token in local storage to keep user logged in between page refreshes
              localStorage.setItem('currentUser', JSON.stringify({ "username":this.username, "token":this.token }));

              // return true to indicate successful login
             resolve(res);
          } else {
              // return false to indicate failed login
              resolve(res);
          }
          //console.log(res['token']);
        resolve(res);
        
      }, (err) => {
        reject(err);
      });
  });

}

logout(): void {
        // clear token remove user from local storage to log user out
        this.token = null;
        localStorage.removeItem('currentUser');
    }

resetpwd(email) {
    const body = new HttpParams()
        .set('email', email);
    return new Promise((resolve, reject) => {
    this.http.post(this.apiUrl+'/reset', body.toString(),{
    headers: { 'Content-Type': 'application/x-www-form-urlencoded' },
     })
    .subscribe(res => {
        resolve(res);
    }, (err) => {
        reject(err);
    });
    });
}

getScreens(token)
{
  
   //console.log(token);
   const body = new HttpParams()
.set('api_token', token)
   return new Promise((resolve, reject) => {
    this.http.post(this.apiUrl+'/screens/get', body.toString(),{
    headers: { 'Content-Type': 'application/x-www-form-urlencoded' },
  })
      .subscribe(res => {
        resolve(res);
      }, (err) => {
        reject(err);
      });
  });
}

addTicket(ticket)
{
  console.log(ticket);
   const body = new HttpParams()
.set('api_token', ticket.api_token)
.set('screen_id', ticket.screen_id)
.set('prob_desc', ticket.prob_desc);
   return new Promise((resolve, reject) => {
    this.http.post(this.apiUrl+'/ticket/add', body.toString(),{
    headers: { 'Content-Type': 'application/x-www-form-urlencoded' },
  })
      .subscribe(res => {
          
        resolve(res);
        
      }, (err) => {
        reject(err);
      });
  });
}

getFiles(token)
{
     //console.log(token);
   const body = new HttpParams()
.set('api_token', token)
   return new Promise((resolve, reject) => {
    this.http.post(this.apiUrl+'/files/troubleshoot', body.toString(),{
    headers: { 'Content-Type': 'application/x-www-form-urlencoded' },
  })
      .subscribe(res => {
        //console.log(res);
        resolve(res);
      }, (err) => {
        reject(err);
      });
  });
}

getHistory(token)
{
     //console.log(token);
   const body = new HttpParams()
.set('api_token', token)
   return new Promise((resolve, reject) => {
    this.http.post(this.apiUrl+'/ticket/client/all', body.toString(),{
    headers: { 'Content-Type': 'application/x-www-form-urlencoded' },
  })
      .subscribe(res => {
        console.log(res);
        resolve(res);
      }, (err) => {
        reject(err);
      });
  });
}

    changePassword(data) {
        const body = new HttpParams()
            .set('password', data.password)
            .set('api_token',this.token)
            .set('con_password', data.con_password);
        return new Promise((resolve, reject) => {
            this.http.post(this.apiUrl+'/user/changepwd', body.toString(),{
                headers: { 'Content-Type': 'application/x-www-form-urlencoded' },
            })
                .subscribe(res => {
                    resolve(res);
                }, (err) => {
                    reject(err);
                });
        });
    }

    changePhone(phone) {
        const body = new HttpParams()
            .set('api_token',this.token)
            .set('phone', phone);
        return new Promise((resolve, reject) => {
            this.http.post(this.apiUrl+'/user/changePhone', body.toString(),{
                headers: { 'Content-Type': 'application/x-www-form-urlencoded' },
            })
                .subscribe(res => {
                    resolve(res);
                }, (err) => {
                    reject(err);
                });
        });
    }

    getChatStatus(){
        const body = new HttpParams()
            .set('api_token',this.token);

        return new Promise((resolve, reject) => {
            this.http.post(this.apiUrl+'/chat/status/get', body.toString(),{
                headers: { 'Content-Type': 'application/x-www-form-urlencoded' },
            })
                .subscribe(res => {
                    resolve(res);
                }, (err) => {
                    reject(err);
                });
        });

    }

    setChatStatus(status){
      if(this.token == null)
          return;

        const body = new HttpParams()
            .set('client','1')
            .set('status',status)
            .set('api_token',this.token);

        return new Promise((resolve, reject) => {
            this.http.post(this.apiUrl+'/chat/status/set', body.toString(),{
                headers: { 'Content-Type': 'application/x-www-form-urlencoded' },
            })
                .subscribe(res => {
                    resolve(res);
                }, (err) => {
                    reject(err);
                });
        });

    }
    getMessages(){
        const body = new HttpParams()
            .set('thread_id', this.thread_id)
            .set('api_token',this.token);

        return new Promise((resolve, reject) => {
            this.http.post(this.apiUrl+'/chat/client/get', body.toString(),{
                headers: { 'Content-Type': 'application/x-www-form-urlencoded' },
            })
                .subscribe(res => {
                    resolve(res);
                }, (err) => {
                    reject(err);
                });
        });

    }

    getTracking(user_id){
    console.log(user_id);
        const body = new HttpParams()
            .set('user_id', user_id)
            .set('api_token',this.token);

        return new Promise((resolve, reject) => {
            this.http.post(this.apiUrl+'/location/get', body.toString(),{
                headers: { 'Content-Type': 'application/x-www-form-urlencoded' },
            })
                .subscribe(res => {
                    resolve(res);
                }, (err) => {
                    reject(err);
                });
        });

    }

    sendMessage(message){
        const body = new HttpParams()
            .set('message',message)
            .set('thread_id',this.thread_id)
            .set('api_token',this.token);

        return new Promise((resolve, reject) => {
            this.http.post(this.apiUrl+'/chat/client/sendMessage', body.toString(),{
                headers: { 'Content-Type': 'application/x-www-form-urlencoded' },
            })
                .subscribe(res => {
                    resolve(res);
                }, (err) => {
                    reject(err);
                });
        });

    }

    getTicketDetails(ticket_id,lat,long){
        const body = new HttpParams()
            .set('ticket_id',ticket_id)
            .set('latitude',lat)
            .set('longitude',long)
            .set('api_token',this.token);

        return new Promise((resolve, reject) => {
            this.http.post(this.apiUrl+'/ticket/getTicket', body.toString(),{
                headers: { 'Content-Type': 'application/x-www-form-urlencoded' },
            })
                .subscribe(res => {
                    resolve(res);
                }, (err) => {
                    reject(err);
                });
        });

    }

    declineTicket(ticket_id){
        const body = new HttpParams()
            .set('ticket_id',ticket_id)
            .set('api_token',this.token);

        return new Promise((resolve, reject) => {
            this.http.post(this.apiUrl+'/ticket/client/decline', body.toString(),{
                headers: { 'Content-Type': 'application/x-www-form-urlencoded' },
            })
                .subscribe(res => {
                    resolve(res);
                }, (err) => {
                    reject(err);
                });
        });

    }
    getTimeSinceArrival(ticket_id){
        const body = new HttpParams()
            .set('ticket_id',ticket_id)
            .set('api_token',this.token);

        return new Promise((resolve, reject) => {
            this.http.post(this.apiUrl+'/ticket/timelog', body.toString(),{
                headers: { 'Content-Type': 'application/x-www-form-urlencoded' },
            })
                .subscribe(res => {
                    resolve(res);
                }, (err) => {
                    reject(err);
                });
        });
    }
    saveFeedBack(ticket_id,feedback,rating){
        const body = new HttpParams()
            .set('ticket_id',ticket_id)
            .set('feedback',feedback)
            .set('rating',rating)
            .set('api_token',this.token);

        return new Promise((resolve, reject) => {
            this.http.post(this.apiUrl+'/ticket/feedback', body.toString(),{
                headers: { 'Content-Type': 'application/x-www-form-urlencoded' },
            })
                .subscribe(res => {
                    resolve(res);
                }, (err) => {
                    reject(err);
                });
        });
    }
    setChatToken(token_id){
        const body = new HttpParams()
            .set('chat_token',token_id)
            .set('api_token',this.token);

        return new Promise((resolve, reject) => {
            this.http.post(this.apiUrl+'/chat/setToken', body.toString(),{
                headers: { 'Content-Type': 'application/x-www-form-urlencoded' },
            })
                .subscribe(res => {
                    resolve(res);
                }, (err) => {
                    reject(err);
                });
        });
    }

    getThread(){
      return this.thread_id;
    }

    closeThread(){
        const body = new HttpParams()
            .set('thread_id', this.thread_id)
            .set('api_token',this.token);

        return new Promise((resolve, reject) => {
            this.http.post(this.apiUrl+'/chat/close', body.toString(),{
                headers: { 'Content-Type': 'application/x-www-form-urlencoded' },
            })
                .subscribe(res => {
                    resolve(res);
                }, (err) => {
                    reject(err);
                });
        });
    }

}
